use std::slice::Iter;
use std::sync::Arc;

use crate::game::{Game, Region, RegionManager, WorldManager};
use crate::util::sync::*;

/**
 * A step is a distinct step in the global tick operation.
 * Steps execute synchronously in order, workers cannot interleave.
 * Steps are strictly read-transmit or write-recieve.
 *
 * right now there's only two distinct steps (and hopefully it'll
 * stay that way) but this file provides a centralized place to
 * create and modify others.
 */
#[derive(Debug, Clone, Copy)]
pub enum Step {
  Process,
  Update
}

type ReadOnly<'a> = RwLockReadGuard<'a, RegionManager>;
type WriteLocal<'a> = RwLockWriteGuard<'a, RegionManager>;
pub enum RichStep<'a> {
  Process(ReadOnly<'a>, &'a Arc<Game>),
  Update(WriteLocal<'a>)
}

impl Step {
  pub fn iterate() -> Iter<'static, Step> {
    [
      Step::Process,
      Step::Update
    ].iter()
  }

  pub fn mode<'a>(&self, game: &'a Arc<Game>, region: &'a Arc<RwLock<RegionManager>>) -> RichStep<'a> {
    match self {
      // Step::PrepareCursors => RichStep::PrepareCursors(region.read().unwrap(), manager),
      // Step::ProcessPreparations => RichStep::ProcessPreparations(region.write().unwrap()),
      // Step::ProcessCursors => RichStep::ProcessCursors(region.read().unwrap(), manager),
      // Step::ApplyUpdates => RichStep::ApplyUpdates(region.write().unwrap())
      Step::Process => RichStep::Process(region.read().unwrap(), game),
      Step::Update => RichStep::Update(region.write().unwrap())
    }
  }

  pub fn name(&self) -> &'static str {
    match self {
      Step::Process => "Process",
      Step::Update => "Update"
    }
  }
}