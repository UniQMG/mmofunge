use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum Direction {
  Up,
  Left,
  Right,
  Down
}

impl Direction {
  pub fn all() -> &'static [Direction; 4] {
    &[Direction::Up, Direction::Left, Direction::Right, Direction::Down]
  }

  /**
   * Returns the vector that this direction is pointing in
   */
  pub fn delta(&self) -> (i64, i64) {
    match self {
      Direction::Up    => ( 0, -1),
      Direction::Left  => (-1,  0),
      Direction::Right => ( 1,  0),
      Direction::Down  => ( 0,  1)
    }
  }

  /**
   * Returns the vector pointing to the left relative to this direction
   */
  pub fn tangential(&self) -> (i64, i64) {
    match self {
      Direction::Up    => ( 1,  0),
      Direction::Left  => ( 0,  1),
      Direction::Right => ( 0, -1),
      Direction::Down  => (-1,  0)
    }
  }
}