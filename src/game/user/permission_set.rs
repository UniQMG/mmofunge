use crate::game::*;

#[derive(Debug)]
pub enum PermissionSet {
  Explicit(UserClaimPermissions),
  CursorClaim(Option<ClaimId>),
  Admin
}

impl PermissionSet {
  pub fn has_perms(&self, dest: Option<ClaimId>, level: ClaimPermission) -> bool {
    if level == ClaimPermission::None { return true }
    match (self, dest) {
      (PermissionSet::Admin, _) => true,

      (PermissionSet::CursorClaim(_source), None) => true,
      (PermissionSet::CursorClaim(Some(source)), Some(dest)) if *source == dest => true,
      (PermissionSet::CursorClaim(None), Some(_dest)) => false,

      (PermissionSet::Explicit(_), None) => true,
      (PermissionSet::Explicit(perms), Some(dest)) if perms.get_perms(dest) >= level => true,
      _ => false
    }
  }
}

#[test]
fn permission_set() {
  let perm = PermissionSet::CursorClaim(Some(ClaimId(0)));
  assert_eq!(perm.has_perms(Some(ClaimId(0)), ClaimPermission::Member), true);
  assert_eq!(perm.has_perms(Some(ClaimId(1)), ClaimPermission::Member), false);
  assert_eq!(perm.has_perms(None, ClaimPermission::Member), true);

  let mut set = UserClaimPermissions::new();
  set.set_perms(ClaimId(0), ClaimPermission::Member);
  let perm = PermissionSet::Explicit(set);
  assert_eq!(perm.has_perms(None, ClaimPermission::Member), true);
  assert_eq!(perm.has_perms(Some(ClaimId(0)), ClaimPermission::Member), true);
  assert_eq!(perm.has_perms(Some(ClaimId(0)), ClaimPermission::Owner), false);
  assert_eq!(perm.has_perms(Some(ClaimId(1)), ClaimPermission::Member), false);

  let perm = PermissionSet::Explicit(UserClaimPermissions::new());
  assert_eq!(perm.has_perms(Some(ClaimId(0)), ClaimPermission::Owner), false);
}