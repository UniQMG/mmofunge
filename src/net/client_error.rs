use std::error::Error;
use std::fmt::{Display, Formatter, Result};
use crate::game::RegionManager;
use std::sync::{RwLock, PoisonError, RwLockReadGuard};
use serde::Serialize;

#[derive(Serialize, Debug)]
pub enum ClientError {
  UsernameAlreadyRegistered,
  NotAuthenticated,
  InvalidCredentials,
  SubscriptionLimitReached,
  Ratelimited,
  OutOfBounds,
  InsufficientPermission,
  ResourceLimitReached,
  AlreadyClaimed,
  IncorrectClaim,
  ProtocolError(String),
  UnparsableMessage(String),
  InternalError(&'static str)
}

impl ClientError {
  pub fn is_fatal(&self) -> bool {
    match self {
      ClientError::ProtocolError(_) => true,
      ClientError::InternalError(_) => true,
      _ => false
    }
  }
}

impl<T> From<std::sync::mpsc::SendError<T>> for ClientError {
  fn from(_err: std::sync::mpsc::SendError<T>) -> Self {
    ClientError::InternalError("Failed to send message to worker thread: Hangup")
  }
}

impl<T> From<PoisonError<T>> for ClientError {
  fn from(_err: PoisonError<T>) -> Self {
    ClientError::InternalError("Attempt to acquire poisoned Mutex or RwLock")
  }
}

impl From<serde_json::Error> for ClientError {
  fn from(err: serde_json::Error) -> Self {
    ClientError::UnparsableMessage(err.to_string())
  }
}

impl From<tungstenite::error::Error> for ClientError {
  fn from(err: tungstenite::error::Error) -> Self {
    ClientError::ProtocolError(err.to_string())
  }
}

impl Display for ClientError {
  fn fmt(&self, f: &mut Formatter<'_>) -> Result {
    write!(f, "{:?}", self)
  }
}

impl Error for ClientError {
  fn description(&self) -> &str {
    "something happened"
  }
}