use crate::game::*;
use crate::net::{ClientError, AuthToken};

#[derive(serde::Serialize)]
pub enum WsPacketOut {
  Reply(Nonce, WsPacketOutInner),
  Error(Nonce, ClientError),
  FatalError(ClientError),
  Update(RegionStateUpdate),
  RatelimitWarning(u64)
}

#[derive(serde::Serialize)]
#[serde(untagged)]
pub enum WsPacketOutInner {
  Success,
  // TODO
  // LoggedIn(AuthToken),
  ClaimCreated(ClaimId)
}
