use arr_macro::arr;
use serde::{Deserialize, Serialize};

use crate::game::*;

pub const REGION_SIZE: usize = 32;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Region {
  #[serde(flatten)]
  pub pos: RegionCoord,
  pub queue: Vec<QueuedAction>,
  pub tiles: [[Tile; REGION_SIZE]; REGION_SIZE]
}

impl Region {
  pub fn tile(&self, pos: LocalTileCoord) -> &Tile {
    &self.tiles[pos.x][pos.y]
  }

  pub fn tile_mut(&mut self, pos: LocalTileCoord) -> &mut Tile {
    &mut self.tiles[pos.x][pos.y]
  }

  pub fn new(pos: RegionCoord) -> Self {
    Region {
      pos,
      queue: vec![],
      tiles: Region::cols()
    }
  }

  fn cols() -> [[Tile; REGION_SIZE]; REGION_SIZE] {
    arr![Region::row(); 32]
  }

  fn row() -> [Tile; REGION_SIZE] {
    arr![Tile::new(); 32]
  }
}
