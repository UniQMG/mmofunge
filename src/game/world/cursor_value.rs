use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, Copy, Eq, PartialEq)]
pub enum CursorValue {
  Integer(i64),
  Null
}

impl CursorValue {
  pub fn truthy(&self) -> bool {
    match self {
      CursorValue::Integer(val) => *val != 0,
      CursorValue::Null => false
    }
  }
}
