use std::collections::HashMap;
use std::sync::{Arc, mpsc};
use std::sync::mpsc::{Receiver, Sender, TryRecvError};

use rand::Rng;

use crate::game::*;
use crate::game::CursorValue::Integer;
use crate::net::ClientError;
use crate::util::LocalIdGenerator;
use crate::util::sync::*;

// extern crate no_deadlocks; use no_deadlocks::{ Mutex }; // Now that's some drop-in debugging

pub struct RegionManager {
  pos: RegionCoord,
  region: Option<Region>,
  pub cursors: HashMap<CursorId, Cursor>,
  worker_rx: Arc<Mutex<Receiver<WorkerMessage>>>,
  worker_tx: Arc<Mutex<Sender<WorkerMessage>>>,
  subs: Arc<Mutex<WorkerSubs>>
}

impl RegionManager {
  pub fn new(pos: RegionCoord) -> Self {
    let (tx, rx) = mpsc::channel::<WorkerMessage>();
    RegionManager {
      pos: pos,
      region: None,
      cursors: HashMap::new(),
      worker_rx: Arc::new(Mutex::new(rx)),
      worker_tx: Arc::new(Mutex::new(tx)),
      subs: Arc::new(Mutex::new(WorkerSubs::new()))
    }
  }

  pub fn id(&self) -> RegionCoord {
    self.pos
  }

  pub fn is_loaded(&self) -> bool {
    self.region.is_some()
  }

  pub fn load_region(&mut self, region: Region) {
    assert_eq!(region.pos, self.pos);
    assert!(self.region.is_none());
    self.region = Some(region);
  }

  pub fn run_step(step: RichStep, args: RegionManagerUpdateArgs) {
    match step {
      RichStep::Process(this, game) => this.tick_process(game),
      RichStep::Update(mut this) => this.tick_update(args)
    }
  }

  /**
   * Runs the process step for the region, which causes it to process internal game logic.
   */
  pub fn tick_process(&self, game: &Game) {
    let manager = &game.world;
    let region = self.region.as_ref().unwrap();

    for action in &region.queue {
      match action {
        QueuedAction::FulfillFetchRequest(region, cursor, tilecontent) => {
          let msg = WorkerMessage::FulfillFetchRequest(*cursor, *tilecontent);
          manager.get_sender(*region).unwrap().send(msg).unwrap();
        },
        QueuedAction::FulfillSyncRequest(region, cursor, value) => {
          let msg = WorkerMessage::FulfillSyncRecieveRequest(*cursor, *value);
          manager.get_sender(*region).unwrap().send(msg).unwrap();
        }
        QueuedAction::RetrySyncRequest(id, return_loc, target) => {
          let msg = WorkerMessage::SyncRecieveRequest(*id, *return_loc, *target);
          self.get_sender().send(msg).unwrap();
        }
      }
    }

    for cursor in self.cursors.values() {
      match cursor.state {
        CursorState::QueuedForDeletion => continue,
        CursorState::Dead(_) => continue,
        _ => {}
      }

      let coord = cursor.pos.to_local(self.pos);
      let tile = region.tile(coord);
      if let TileMaterial::Water(_) = tile.material {
        let mut cursor = cursor.clone();
        cursor.kill(DeathReason::Drowned);
        self.move_cursor(manager, cursor);
        continue;
      }

      match cursor.state {
        CursorState::Dead(_) => unreachable!(),
        CursorState::QueuedForDeletion => unreachable!(),
        CursorState::FetchRequestBlock => {},
        CursorState::SyncReadBlock(_) => {},
        CursorState::SyncWriteBlock(_) => {},
        CursorState::Normal => {
          let mut cursor = cursor.clone();
          self.step_cursor_normal(manager, &mut cursor);
          self.move_cursor(manager, cursor);
        },
        CursorState::StringMode => {
          let mut cursor = cursor.clone();
          let coord = cursor.pos.to_local(self.pos);
          let content = region.tile(coord).content;
          if let TileContent::Character('\"') = content {
            cursor.state = CursorState::Normal;
          } else {
            cursor.sepushku(CursorValue::Integer(match content {
              TileContent::Character(char) => char,
              TileContent::None => ' '
            } as i64));
          }
          cursor.step();
          self.move_cursor(manager, cursor);
        },
        CursorState::IntegerMode => {
          let mut cursor = cursor.clone();
          let coord = cursor.pos.to_local(self.pos);
          let content = region.tile(coord).content;
          if let TileContent::Character('\'') = content {
            cursor.state = CursorState::Normal;
          } else {
            match content.into() {
              Instruction::PushInteger(next_digit) => {
                cursor.sepopku(&|cs, val| {
                  cs.sepushku(match val {
                    CursorValue::Integer(value) => CursorValue::Integer(value*10 + next_digit as i64),
                    _ => { cs.kill(DeathReason::TypeMismatch); return; }
                  });
                });
                if let CursorState::Dead(_) = cursor.state {
                  continue;
                }
              },
              _ => {
                cursor.kill(DeathReason::TypeMismatch);
                continue
              }
            }
          }
          cursor.step();
          self.move_cursor(manager, cursor);
        }
      }
    }
  }

  /**
   * Performs a cursor step in 'normal' mode, executing instructions under it.
   */
  fn step_cursor_normal(&self, manager: &WorldManager, cursor: &mut Cursor) {
    let region = self.region.as_ref().unwrap();

    let coord = cursor.pos.to_local(self.pos);
    let cursor_tile = region.tile(coord);
    match cursor_tile.content.into() {
      Instruction::DirUp => cursor.dir = Direction::Up,
      Instruction::DirLeft => cursor.dir = Direction::Left,
      Instruction::DirRight => cursor.dir = Direction::Right,
      Instruction::DirDown => cursor.dir = Direction::Down,
      Instruction::DirRandom => {
        cursor.dir = match rand::thread_rng().gen_range(0..4) {
          0 => Direction::Up,
          1 => Direction::Left,
          2 => Direction::Right,
          _ => Direction::Down
        };
      },
      Instruction::PushInteger(val) => cursor.sepushku(CursorValue::Integer(val as i64)),
      Instruction::Add => cursor.fold_two_ints(&|a, b| b + a),
      Instruction::Sub => cursor.fold_two_ints(&|a, b| b - a),
      Instruction::Mul => cursor.fold_two_ints(&|a, b| b * a),
      Instruction::Div => cursor.fold_two_ints(&|a, b| b / a),
      Instruction::Mod => cursor.fold_two_ints(&|a, b| b % a),
      Instruction::LogicalNot => cursor.sepopku(&|cs, val| {
        cs.sepushku(Integer(if val.truthy() { 1 } else { 0 }))
      }),
      Instruction::GreaterThan => cursor.double_sepopku(&|cs, a, b| {
        match (a, b) {
          (Integer(a), Integer(b)) => cs.sepushku(Integer(if b > a { 1 } else { 0 })),
          _ => cs.kill(DeathReason::TypeMismatch)
        };
      }),
      Instruction::HorizontalIf => cursor.sepopku(&|cs, val| {
        cs.dir = if val.truthy() { Direction::Left } else { Direction::Right };
      }),
      Instruction::VerticalIf => cursor.sepopku(&|cs, val| {
        cs.dir = if val.truthy() { Direction::Up } else { Direction::Down };
      }),
      Instruction::StringMode => cursor.state = match cursor.state {
        CursorState::Normal => CursorState::StringMode,
        other => other
      },
      Instruction::StackDuplicate => cursor.sepopku(&|cs, val| {
        cs.sepushku(val);
        cs.sepushku(val);
      }),
      Instruction::StackSwap => cursor.double_sepopku(&|cs, a, b| {
        cs.sepushku(a);
        cs.sepushku(b);
      }),
      Instruction::StackDiscard => cursor.sepopku(&|_cs, _val|{}),
      Instruction::StackOutputInteger => cursor.sepopku(&|cs, val| {
        let mut subs = self.subs.lock().unwrap();
        subs.notify(RegionStateUpdate::CursorMessage(cs.id, format!("{:?}", val)));
      }),
      Instruction::StackOutputChar => cursor.sepopku(&|cs, val| {
        let mut subs = self.subs.lock().unwrap();
        subs.notify(RegionStateUpdate::CursorMessage(cs.id, format!("{}", match val {
          Integer(val) if val <= 255 => val as u8 as char,
          _ => '?'
        })));
      }),
      Instruction::Bridge => cursor.step(),
      Instruction::Get => {
        cursor.double_sepopku(&|cs, y, x| {
          let (x, y) = match (x, y) {
            (Integer(x), Integer(y)) => (x, y),
            _ => {
              cs.kill(DeathReason::TypeMismatch);
              return;
            }
          };
          cs.state = CursorState::FetchRequestBlock;
          let target = GlobalTileCoord { x, y };
          let sender = match manager.get_sender(target.to_region()) {
            None => {
              cs.kill(DeathReason::OutOfBounds);
              return;
            },
            Some(sender) => sender
          };
          // step the cursor now instead of later just in case it
          // moves over a region border
          cs.step();
          sender.send(WorkerMessage::FetchRequest(cs.id, cs.pos.to_region(), target)).unwrap();
        });
      },
      Instruction::Put => {
        cursor.triple_sepopku(&|cs, y, x, val| {
          let tile = match (x, y) {
            (Integer(x), Integer(y)) => GlobalTileCoord { x, y },
            _ => {
              cs.kill(DeathReason::TypeMismatch);
              return;
            }
          };

          let sender = match manager.get_sender(tile.to_region()) {
            Some(sender) => sender,
            None => {
              cs.kill(DeathReason::OutOfBounds);
              return;
            }
          };

          sender.send(WorkerMessage::Set(
            tile.to_local(tile.to_region()),
            match val {
              Integer(val) => TileContent::Character(val as u8 as char),
              CursorValue::Null => TileContent::Character(' ')
            },
            PermissionSet::CursorClaim(cursor_tile.claim),
            None
          )).unwrap();
        });
      },
      Instruction::StackInputInteger => todo!(),
      Instruction::StackInputChar => todo!(),
      Instruction::Terminate => cursor.kill(DeathReason::Terminated),

      Instruction::GetPosition => {
        cursor.sepushku(CursorValue::Integer(cursor.pos.x));
        cursor.sepushku(CursorValue::Integer(cursor.pos.y));
      },
      Instruction::IntegerMode => {
        cursor.state = match cursor.state {
          CursorState::Normal => {
            cursor.sepushku(CursorValue::Integer(0));
            CursorState::IntegerMode
          },
          other => other
        }
      },

      Instruction::SyncSend => {
        cursor.sepopku(&|cs, val| {
          cs.state = CursorState::SyncWriteBlock(val);
        });
      },
      Instruction::SyncRecieve => {
        cursor.state = CursorState::SyncReadBlock(4);

        for dir in Direction::all() {
          let target = cursor.pos + dir.delta().into();
          let sender = match manager.get_sender(target.to_region()) {
            None => {
              cursor.kill(DeathReason::OutOfBounds);
              return;
            },
            Some(sender) => sender
          };
          sender.send(WorkerMessage::SyncRecieveRequest(cursor.id, cursor.pos, target)).unwrap();
        }
      },

      Instruction::Comment(_) => {}
    };

    match cursor.state {
      CursorState::Normal => cursor.step(),
      CursorState::StringMode => cursor.step(),
      CursorState::IntegerMode => cursor.step(),
      CursorState::FetchRequestBlock => {},
      CursorState::QueuedForDeletion => {},
      CursorState::SyncReadBlock(_) => {},
      CursorState::SyncWriteBlock(_) => {},
      CursorState::Dead(_) => {}
    }
  }

  /**
   * Runs the update step for the region, which causes it to process messages sent by
   * other regions during their process step and apply them.
   */
  pub fn tick_update(&mut self, args: RegionManagerUpdateArgs) {
    let region = self.region.as_mut().unwrap();
    // The queue is processed (but can't be cleared) during the 'process' step
    // It's only written to during the 'update' step, so clear it now
    region.queue.clear();

    let mut subs = self.subs.lock().unwrap();
    let rx = self.worker_rx.lock().unwrap();
    loop {
      match rx.try_recv() {
        Err(TryRecvError::Disconnected) => unreachable!(),
        Err(TryRecvError::Empty) => break,
        Ok(WorkerMessage::Set(tilepos, content, perms, confirm)) => {
          let tile = region.tile_mut(tilepos);
          if !perms.has_perms(tile.claim, ClaimPermission::Member) {
            if let Some(confirm) = confirm {
              confirm.fail(ClientError::InsufficientPermission);
            }
            continue;
          }

          if tile.content != content {
            tile.content = content;
            subs.notify(RegionStateUpdate::Tile(
              self.pos,
              tilepos,
              region.tile_mut(tilepos).clone()
            ));
          }
          if let Some(confirm) = confirm {
            confirm.confirm(())
          };
        }
        Ok(WorkerMessage::GetState(sender)) => {
          let _ = sender.send(region.clone());
        },
        Ok(WorkerMessage::Subscribe(sender)) => {
          let _ = sender.send(RegionStateUpdate::Full(region.clone(), self.cursors.clone()));
          subs.register(sender);
        }
        Ok(WorkerMessage::FetchRequest(cursor, return_region, tile)) => {
          region.queue.push(QueuedAction::FulfillFetchRequest(
            return_region,
            cursor,
            region.tile(tile.to_local(self.pos)).content
          ));
        }
        Ok(WorkerMessage::FulfillFetchRequest(cursor, tilecontent)) => {
          let cursor = self.cursors.get_mut(&cursor).unwrap();
          assert_eq!(cursor.state, CursorState::FetchRequestBlock);
          cursor.stack.push(match tilecontent {
            TileContent::Character(char) => CursorValue::Integer(char as i64),
            TileContent::None => CursorValue::Integer(' ' as i64)
          });
          cursor.state = CursorState::Normal;
        }
        Ok(WorkerMessage::CreateCursor(state, pos, dir, stack)) => {
          let id = CursorId(args.id_generator.generate());
          let cursor = Cursor { id, state, pos, dir, stack };
          self.cursors.insert(cursor.id, cursor.clone());
          subs.notify(RegionStateUpdate::Cursor(cursor));
        }
        Ok(WorkerMessage::UpsertCursor(cursor)) => {
          self.cursors.insert(cursor.id, cursor.clone());
          subs.notify(RegionStateUpdate::Cursor(cursor));
        }
        Ok(WorkerMessage::MoveCursorOut(id)) => {
          self.cursors.remove(&id);
          subs.notify(RegionStateUpdate::CursorDeleted(id));
        },
        Ok(WorkerMessage::DeleteCursor(id)) => {
          if let Some(cs) = self.cursors.get_mut(&id) {
            cs.state = CursorState::QueuedForDeletion;
          }
          subs.notify(RegionStateUpdate::CursorDeleted(id));
        }
        Ok(WorkerMessage::Claim(tile, claimant, claim, confirm)) => {
          let pos = tile.to_local(self.pos);
          let tile = &mut region.tiles[pos.x][pos.y];
          match tile.claim {
            Some(_) => confirm.fail(ClientError::AlreadyClaimed),
            None => {
              tile.claim = Some(claim);
              confirm.confirm(());
              subs.notify(RegionStateUpdate::Tile(self.pos, pos, tile.clone()));
            }
          }
        }
        Ok(WorkerMessage::Unclaim(tile, claimant, claim, confirm)) => {
          let pos = tile.to_local(self.pos);
          let tile = &mut region.tiles[pos.x][pos.y];
          match tile.claim {
            Some(existing) if existing == claim => {
              tile.claim = None;
              confirm.confirm(());
              subs.notify(RegionStateUpdate::Tile(self.pos, pos, tile.clone()));
            },
            Some(_) => confirm.fail(ClientError::IncorrectClaim), // Not our claim!
            None => confirm.fail(ClientError::IncorrectClaim) // Not claimed
          }
        },
        Ok(WorkerMessage::SyncRecieveRequest(id, return_loc, target)) => {
          let tile = region.tile(target.to_local(self.pos));
          if Instruction::SyncSend != tile.content.into() {
            let res = SyncRequestResponse::NoInstruction;
            region.queue.push(QueuedAction::FulfillSyncRequest(return_loc.into(), id, res));
            continue;
          }

          let response = self.cursors
            .values_mut()
            .filter_map(|mut cs| {
              match cs.state {
                CursorState::SyncWriteBlock(val) if target == cs.pos => {
                  cs.state = CursorState::Normal;
                  cs.step();
                  Some(val)
                },
                _ => None
              }
            })
            .next();

          region.queue.push(match response {
            Some(val) => {
              let res = SyncRequestResponse::Success(val);
              QueuedAction::FulfillSyncRequest(return_loc.into(), id, res)
            },
            None => {
              QueuedAction::RetrySyncRequest(id, return_loc, target)
            }
          });
        }
        Ok(WorkerMessage::FulfillSyncRecieveRequest(cursor, value)) => {
          let cursor = match self.cursors.values_mut().find(|cs| cs.id == cursor) {
            Some(cursor) => cursor,
            _ => continue
          };

          cursor.state = match cursor.state {
            CursorState::SyncReadBlock(n) if n > 1 => CursorState::SyncReadBlock(n - 1),
            CursorState::SyncReadBlock(1) => {
              cursor.step();
              CursorState::Normal
            },
            CursorState::Normal => continue,
            CursorState::QueuedForDeletion => continue,
            e => unreachable!("Unexpected cursor state: {:?}", e)
          };

          match value {
            SyncRequestResponse::NoInstruction => {
              // There was no send instruction there, so there'll never be a value returned.
            }
            SyncRequestResponse::Success(val) => {
              cursor.sepushku(val);
            }
          }
        }
      }
    }
  }

  fn move_cursor(&self, manager: &WorldManager, mut cursor: Cursor) {
    self.send(WorkerMessage::MoveCursorOut(cursor.id)).unwrap();
    match manager.get_sender(cursor.pos.into()) {
      Some(sender) => {
        sender.send(WorkerMessage::UpsertCursor(cursor)).unwrap();
      },
      None => {
        cursor.kill(DeathReason::OutOfBounds);
        self.send(WorkerMessage::UpsertCursor(cursor)).unwrap();
      }
    }
  }

  pub fn send(&self, msg: WorkerMessage) -> Result<(), ()> {
    match self.worker_tx.lock().unwrap().send(msg) {
      Ok(()) => Ok(()),
      Err(_) => Err(())
    }
  }

  pub fn can_unload(&self) -> bool {
    let has_incoming_packets = match self.worker_rx.lock().unwrap().try_recv() {
      Err(_) => false,
      Ok(pkt) => {
        self.send(pkt).unwrap(); // requeue packet
        true
      }
    };
    // TODO: This is probably a terrible idea which will result in lost packets = client sees errors
    self.cursors.is_empty() && self.subs.lock().unwrap().subcount() == 0 && !has_incoming_packets
  }

  pub fn get_sender(&self) -> Sender<WorkerMessage> {
    self.worker_tx.lock().unwrap().clone()
  }

  pub fn borrow_region(&self) -> &Region {
    self.region.as_ref().unwrap()
  }
}
