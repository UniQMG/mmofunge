use std::sync::Arc;

use crate::game::Region;
use crate::util::LocalIdGenerator;

pub struct RegionManagerUpdateArgs<'a> {
  pub id_generator: &'a LocalIdGenerator
}