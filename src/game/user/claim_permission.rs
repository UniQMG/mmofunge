#[derive(Debug, Copy, Clone, Ord, Eq, PartialOrd, PartialEq, serde::Serialize, serde::Deserialize)]
pub enum ClaimPermission {
  /**
   * Not an actual permission level, but implies the lack of other permission levels
   */
  None,
  /**
   * A claim member can edit in the claim
   */
  Member,
  /**
   * A manager can claim or unclaim tiles (+user perms)
   */
  Manager,
  /**
   * The owner of a claim can add/remove other users to the claim (+manager perms)
   */
  Owner
}

impl ClaimPermission {
  pub fn is_at_least(&self, level: ClaimPermission) -> bool {
    *self >= level
  }
}