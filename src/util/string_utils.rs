#[allow(clippy::eq_op)] // Alignment and readability > ... there's literally no downside. Go away.

pub enum BarStyle {
  Eighths,
  Shades
}

pub fn bar(steps: usize, fill: f64, style: BarStyle) -> String {
  let mut str = String::with_capacity(steps);
  for i in 0..steps {
    let step_progress = (fill * steps as f64 - i as f64).clamp(0.0, 1.0);
    str.push(match style {
      BarStyle::Eighths if step_progress >= 8.0/8.0 => '█',
      BarStyle::Eighths if step_progress >= 7.0/8.0 => '▉',
      BarStyle::Eighths if step_progress >= 6.0/8.0 => '▊',
      BarStyle::Eighths if step_progress >= 5.0/8.0 => '▋',
      BarStyle::Eighths if step_progress >= 4.0/8.0 => '▌',
      BarStyle::Eighths if step_progress >= 3.0/8.0 => '▍',
      BarStyle::Eighths if step_progress >= 2.0/8.0 => '▎',
      BarStyle::Eighths if step_progress >= 1.0/8.0 => '▏',
      BarStyle::Shades  if step_progress >= 4.0/4.0 => '█',
      BarStyle::Shades  if step_progress >= 3.0/4.0 => '▓',
      BarStyle::Shades  if step_progress >= 2.0/4.0 => '▒',
      BarStyle::Shades  if step_progress >= 1.0/4.0 => '░',
      _ => ' '
    });
  };
  str
}

#[test]
fn test_bar() {
  assert_eq!(bar(10, 1.000, BarStyle::Eighths), "██████████");
  assert_eq!(bar(10, 0.500, BarStyle::Eighths), "█████     ");
  assert_eq!(bar(10, 0.575, BarStyle::Eighths), "█████▊    "); // reeeee
  assert_eq!(bar(10, 0.550, BarStyle::Eighths), "█████▌    ");
  assert_eq!(bar(10, 0.000, BarStyle::Eighths), "          ");
}
