use std::collections::HashMap;
use std::ops::DerefMut;
use std::sync::{Arc, mpsc};
use std::sync::mpsc::{Receiver, RecvError, Sender, TryRecvError};
use std::thread::{Builder, JoinHandle, spawn};
use std::time::Instant;

use mongodb::{bson, bson::{Bson, doc}, Database};
use mongodb::error::Error;
use mongodb::options::{FindOptions, UpdateOptions};
use mongodb::results::UpdateResult;
use tokio::stream::StreamExt;

use crate::game::*;
use crate::util::{IdGenerator, LocalIdGenerator, sync::*};
use crate::util::database_manager::DatabaseManager;

/** Maximum size of the world. Valid coordinates are -WORLD_SIZE to WORLD_SIZE */
const DEFAULT_WORLD_SIZE: i64 = 128;

pub struct WorldManager {
  /** A list of regions managed by this world manager. */
  regions: RwLock<HashMap<RegionCoord, RegionInfo>>,
  /** A list of workers which own region workers which own live instances of regions */
  worker_pool: RwLock<Vec<RegionWorkerThread>>,
  /** A list of regions that need to be assigned to a worker next tick */
  queued_workers: RwLock<Vec<Arc<RwLock<RegionManager>>>>,
  dbm: Arc<DatabaseManager>
}

impl WorldManager {
  pub fn new(db: Arc<DatabaseManager>) -> WorldManager {
    WorldManager {
      regions: RwLock::new(HashMap::new()),
      worker_pool: RwLock::new(vec![]),
      queued_workers: RwLock::new(Vec::new()),
      dbm: db
    }
  }

  pub fn spawn(&self, game: &Arc<Game>, thread_count: usize) {
    let mut pool = self.worker_pool.write().unwrap();
    assert!(pool.is_empty());
    for id in 0..thread_count {
      pool.push(RegionWorkerThread::new(id, game.clone()))
    }
    drop(pool);
  }

  pub fn region_count(&self) -> usize {
    self.regions.read().unwrap().len()
  }

  /**
   * Loads a region into memory, either getting it from the database or creating it.
   * BUG: Regions can be fetched after unloading but before becoming visible from the database
   *      while a save job is running. This ends up with them being reset. Regions pending
   *      an unload operation need to be kept around until the save job is done.
   *
   */
  fn load_region(&self, pos: RegionCoord) {
    // Create and insert empty region
    let region = RegionInfo::new(RegionManager::new(pos));
    let manager = region.manager.clone();

    let mut queued_workers = self.queued_workers.write().unwrap();
    let mut regions = self.regions.write().unwrap();

    // Someone tried to call load_region inbetween calls of load_region
    // Waste of locks but not really an error
    if regions.contains_key(&region.id()) {
      return;
    }
    regions.insert(region.id(), region);
    queued_workers.push(manager.clone());

    drop(regions);
    drop(queued_workers);

    let dbm = self.dbm.clone();
    self.dbm.background(async move {
      // Perform actual region loading
      let region_query = dbm.database.collection("regions").find_one(
        Some(doc!{ "x": pos.x, "y": pos.y }),
        None
      ).await;
      let region = match region_query {
        Err(err) => panic!("Database error {:?}", err),
        Ok(None) => {
          // todo: improve
          WorldGenerator::new().generate(pos)
        },
        Ok(Some(val)) => {
          match bson::from_bson(Bson::Document(val)) {
            Err(err) => panic!("Database error {:?}", err),
            Ok(val) => val
          }
        }
      };

      let min = LocalTileCoord { x: 0, y: 0 }.to_global(pos);
      let max = LocalTileCoord { x: REGION_SIZE-1, y: REGION_SIZE-1 }.to_global(pos);
      let cursor_query = dbm.database.collection("cursors").find(
        Some(doc!{
          "x": { "$gte": min.x, "$lte": max.x },
          "y": { "$gte": min.y, "$lte": max.y }
        }),
        None
      ).await;

      let mut cursors = vec![];
      match cursor_query {
        Err(err) => panic!("Database error {:?}", err),
        Ok(mut query) => {
          while let Some(cursor) = query.next().await {
            let cursor: Cursor = match bson::from_bson(Bson::Document(cursor.unwrap())) {
              Err(err) => panic!("Database error {:?}", err),
              Ok(val) => val
            };
            cursors.push(cursor);
          }
        }
      };

      // Load in the moved region
      let mut manager = manager.write().unwrap();
      for cursor in cursors {
        manager.cursors.insert(cursor.id, cursor);
      }
      manager.load_region(region);
    });
  }

  fn get_from_region<T>(&self, pos: RegionCoord, cb: &dyn Fn(&RegionInfo) -> T) -> Option<T> {
    // World boundaries check
    let ws = DEFAULT_WORLD_SIZE;
    if pos.x > ws || pos.y > ws || pos.x < -ws || pos.y < -ws {
      return None;
    }

    let regions = self.regions.read().unwrap();
    if let Some(regioninfo) = regions.get(&pos) {
      return Some(cb(regioninfo));
    }
    drop(regions);

    self.load_region(pos);
    Some(cb(self.regions.read().unwrap().get(&pos).unwrap()))
  }

  /**
   * Fetches a region, either getting it from the database or creating it. A None will be returned
   * only when the region cannot exist (e.g. out of bounds).
   * Should not be used from region workers.
   */
  pub fn fetch_region(&self, pos: RegionCoord) -> Option<Arc<RwLock<RegionManager>>> {
    self.get_from_region(pos, &|reg| reg.manager.clone())
  }

  /**
   * Gets a Sender to send messages to a region
   */
  pub fn get_sender(&self, pos: RegionCoord) -> Option<Sender<WorkerMessage>> {
    self.get_from_region(pos, &|reg| (*reg.sender).lock().unwrap().clone())
  }
}

impl GameAspect for WorldManager {
  fn tick(&self) {
    let mut queued_workers = self.queued_workers.write().unwrap();
    for manager in queued_workers.drain(0..) {
      let unwrapped = manager.read().unwrap();
      let id = unwrapped.id();
      let poolsize = self.worker_pool.read().unwrap().len();
      let bucket = ((id.x + id.y).abs() as usize) % poolsize;

      let mut worker_pool = self.worker_pool.write().unwrap();
      let worker = worker_pool.get_mut(bucket).unwrap();
      self.regions.write().unwrap().get_mut(&id).unwrap().assign_to_worker(worker);
    }
    drop(queued_workers);

    let workers = self.worker_pool.read().unwrap();
    for step in Step::iterate() {
      let mut joiners = workers.iter()
        .map(|worker| worker.run_step(*step))
        .collect::<Vec<Box<dyn FnOnce()>>>();
      loop {
        match joiners.pop() {
          None => break,
          Some(cb) => cb()
        }
      }
    }
    drop(workers);
  }

  fn resurrect(&self) {
    let query = self.dbm.sync(self.dbm.database.collection("cursors").find(
      Some(doc!{ "state.Dead": { "$ne": "Terminated" } }),
      FindOptions::builder()
        .projection(doc! { "_id": 0, "x": 1, "y": 1 })
        .sort(doc! { "x": 1, "y": 1 })
        .build()
    )).unwrap();
    let query = self.dbm.sync_str(query);

    let mut coords = vec![];
    for result in query {
      coords.push(match result {
        Err(e) => panic!("{}", e),
        Ok(doc) => GlobalTileCoord {
          x: doc.get_i64("x").unwrap(),
          y: doc.get_i64("y").unwrap()
        }.to_region()
      });
    }

    coords.dedup();
    for (i, region) in coords.iter().enumerate() {
      println!("[{:>4}/{:>4}] Region {} {}", i, coords.len(), region.x, region.y);
      self.load_region(*region);
    }
    drop(coords);
  }

  fn save(&self) -> Box<dyn SaveJob + Send> {
    let mut savejob = Box::new(MultiSaveJob::new(self.region_count()));

    let (job_tx, job_rx) = mpsc::channel::<RegionSaveJob>();

    let workers = self.worker_pool.read().unwrap();
    for worker in workers.iter() {
      worker.save(job_tx.clone());
    }

    drop(job_tx);
    while let Ok(job) = job_rx.recv() {
      savejob.push(job);
    }

    savejob
  }

  fn post_save(&self) {
    let (unload_tx, unload_rx) = mpsc::channel::<RegionCoord>();

    let workers = self.worker_pool.read().unwrap();
    for worker in workers.iter() {
      worker.post_save(unload_tx.clone());
    }

    drop(unload_tx);
    let mut regions = self.regions.write().unwrap();
    while let Ok(pos) = unload_rx.recv() {
      regions.remove(&pos);
    }
  }
}