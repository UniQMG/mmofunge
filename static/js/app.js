import {REGION_SIZE_TILES} from './constants.js';
import WorldManager from './worldmanager.js';
import CursorSidebar from './components/cursor-sidebar.js';
import UserSidebar from './components/user-sidebar.js';
const html = arg => arg.join('');

function is_empty(cellcontent) {
  return cellcontent == 'None' || /^\s$/.test(cellcontent.Character);
}

const app = new Vue({
  template: html`
    <div id="app">
      <div class="main-view" ref="region_container">
        <div class="action-bar">
          <span>Sidebar</span><!--
          --><button
            @click="active_tab = null; hoveredCursor = null"
            :class="{ active: valid_active_tab == null }"
            :disabled="valid_active_tab == null"
          >x</button><!--
          --><button
            @click.capture.stop="active_tab = 'user'"
            :class="{ active: valid_active_tab == 'user' }"
            :disabled="valid_active_tab == 'user'"
          >user</button><!--
          --><button
            @click.stop="active_tab = 'cursor'"
            :class="{ active: valid_active_tab == 'cursor' }"
            :disabled="valid_active_tab == 'cursor' || !hoveredCursor"
          >cursor</button>
          <span>Tool</span><!--
          --><button
            @click="tool = 'pan'"
            :class="{ active: tool == 'pan' }"
            :disabled="tool == 'pan'"
          >pan</button><!--
          --><button
            disabled="disabled"
            :class="{ active: tool == 'claim' }"
          >claim</button>
        </div>
        <canvas
          ref="canvas"
          :style="style"
          :width="windowDim[0]"
          :height="windowDim[1]"
        />
        <div class="status-text">
          {{ statusText }} |
          <a href="#" @click.prevent="dark_theme = !dark_theme">theme</a>
        </div>
      </div>
      <form v-if="!world || !world.user" class="login" @submit.prevent>
        <div>
          <label for="username">Username</label>
          <input name="username" v-model="username" type="text" />
        </div>
        <div>
          <label for="password">Password</label>
          <input name="password" v-model="password" type="password" />
        </div>
        <div>
          <button @click="login">log in</button>
          <button @click="register">register</button>
        </div>
      </form>
      <div class="alerts">
        <div class="alert" v-for="alert of alerts" :style="{'--timer': alert.timer}">
          {{ alert.text }}
        </div>
      </div>
      <cursor-sidebar
        v-if="valid_active_tab == 'cursor'"
        :cursor="hoveredCursor"
        @delete="deleteCursor(hoveredCursor); hoveredCursor = null"
        @close="hoveredCursor = null"
      />
      <user-sidebar
        v-if="valid_active_tab == 'user'"
        :world="world"
        @close="active_tab = null"
        @setTool="setTool"
      />
    </div>
  `,
  components: { CursorSidebar, UserSidebar },
  data: () => ({
    username: "UniQMG",
    password: "password",

    alerts: [],

    scroll_x: 0,
    scroll_y: 0,
    tile_size_px: 32,
    scroll_amount: Math.sqrt(32),

    set_hash_timeout: null,
    dark_theme: false,
    active_tab: null,

    world: null,
    hoveredCursor: null,

    tool: 'pan',
    toolArgs: null,
    lastDrag: null,

    hover: { x: 0, y: 0 }, // global tile x y
    cursor: [0, 0, 0, 0], // region x y tile x y
    charsTyped: 0,
    windowDim: [window.innerWidth, window.innerHeight]
  }),
  mounted() {
    document.addEventListener('keydown', evt => {
      this.keydown(evt);
    });
    window.addEventListener('resize', () => {
      this.windowDim = [window.innerWidth, window.innerHeight];
    });
    window.addEventListener('wheel', evt => {
      this.scroll_amount += -evt.deltaY / 100 * 0.25;
      if (this.scroll_amount <  2) this.scroll_amount =  2;
      if (this.scroll_amount > 15) this.scroll_amount = 15;

      let old_coord = this.eventToExactCoordinates(evt);
      this.tile_size_px = this.scroll_amount**2 * Math.sign(this.scroll_amount);
      let new_coord = this.eventToExactCoordinates(evt);

      this.scroll_x += (new_coord[0] - old_coord[0]) * this.tile_size_px;
      this.scroll_y += (new_coord[1] - old_coord[1]) * this.tile_size_px;
    });
    window.addEventListener('hashchange', () => {
      this.reloadHash();
    });
    this.reloadHash();

    // Refresh the hovered cursor since its changed from outside vue-land
    setInterval(() => {
      if (!this.hoveredCursor) return;
      let fresh = this.world.cursors.get(this.hoveredCursor.id);
      this.hoveredCursor = fresh || this.hoveredCursor;
    }, 16);

    this.world = new WorldManager(this.$refs.canvas);

    interact(this.$refs.canvas)
      .draggable({ listeners: { move: (evt) => this.drag(evt) } })
      .gesturable({ onmove: (evt) => { console.log(evt.scale); } })
      .on('tap', evt => this.tap(evt))
      .on('doubletap', evt => {
        let [rx, ry, tx, ty] = this.eventToCoordinates(evt);
        this.spawnCursor(rx, ry, tx, ty);
      })
      .on('move', evt => {
        let [rx, ry, tx, ty] = this.eventToCoordinates(evt);
        this.hover.x = rx * REGION_SIZE_TILES + tx;
        this.hover.y = ry * REGION_SIZE_TILES + ty;
      });
  },
  watch: {
    scroll_x() {
      this.resetHash();
      this.world.scroll_x = this.scroll_x;
      this.world.recomputeActiveRegions();
      this.world.markDirty();
    },
    scroll_y() {
      this.resetHash();
      this.world.scroll_y = this.scroll_y;
      this.world.recomputeActiveRegions();
      this.world.markDirty();
    },
    tile_size_px() {
      this.world.tile_size_px = this.tile_size_px;
      this.world.recomputeActiveRegions();
      this.world.markDirty(true);
    },
    hover: {
      deep: true,
      handler() {
        for (let cursor of this.world.cursors.values()) {
          if (cursor.state == 'QueuedForDeletion') continue;
          if (cursor.x == this.hover.x && cursor.y == this.hover.y) {
            this.hoveredCursor = cursor;
            return;
          }
        }
      }
    }
  },
  computed: {
    valid_active_tab() {
      switch (this.active_tab) {
        case 'user':
          return 'user';

        case null: case 'cursor': default:
          return this.hoveredCursor ? 'cursor' : null;
      }
    },
    statusText() {
      let regions = this.world ? this.world.regions.size : -1;
      let cx = this.cursor[0] * REGION_SIZE_TILES + this.cursor[2];
      let cy = this.cursor[1] * REGION_SIZE_TILES + this.cursor[3];
      let data = this.world ? this.world.ws.data_report() : null;
      return [
        `${regions} regions loaded`,
        `Cursor: ${cx} ${cy}`,
        `Mouse: ${this.hover.x} ${this.hover.y}`,
        data
      ].filter(e => e).join(' | ');
    },
    style() {
      return { filter: this.dark_theme ? 'invert()' : '' };
    }
  },
  methods: {
    eventToExactCoordinates(evt) {
      return [
        ((evt.clientX - this.scroll_x) / this.tile_size_px),
        ((evt.clientY - this.scroll_y) / this.tile_size_px)
      ];
    },
    eventToCoordinates(evt) {
      let [gtx, gty] = this.eventToExactCoordinates(evt).map(v => Math.floor(v));
      return [
        Math.floor(gtx / REGION_SIZE_TILES),
        Math.floor(gty / REGION_SIZE_TILES),
        ((gtx % REGION_SIZE_TILES) + 32) % 32,
        ((gty % REGION_SIZE_TILES) + 32) % 32
      ];
    },
    reloadHash() {
      let [a1, a2] = window.location.hash.slice(1).split(',').map(x => parseInt(x));
      if (!isNaN(a1) && !isNaN(a2)) {
        this.scroll_x = a1 * this.tile_size_px +  window.innerWidth/2;
        this.scroll_y = a2 * this.tile_size_px + window.innerHeight/2;
      }
    },
    resetHash() {
      clearTimeout(this.set_hash_timeout);
      this.set_hash_timeout = setTimeout(() => {
        let hash = '#' + [
          Math.floor((this.scroll_x -  window.innerWidth/2) / this.tile_size_px),
          Math.floor((this.scroll_y - window.innerHeight/2) / this.tile_size_px)
        ].join(',');
        if (window.location.hash == hash) return;
        history.replaceState(undefined, undefined, hash);
      }, 50);
    },
    async updateTile(rx, ry, tx, ty, char) {
      let content = char ? { Character: char } : 'None';
      let pr = this.world.ws.send({
        Update: [[rx, ry], [tx, ty], content]
      });

      let regioninfo = await this.world.fetchRegionCt(rx, ry);
      if (!regioninfo) return 'None';
      let old = regioninfo.data.tiles[tx][ty].content;
      regioninfo.data.tiles[tx][ty].content = content;
      regioninfo.data.tiles[tx][ty].predicted = true;
      this.world.markDirty(rx, ry, tx, ty);
      pr.then(() => {
        regioninfo.data.tiles[tx][ty].predicted = false;
        this.world.markDirty(rx, ry, tx, ty);
      }).catch(ex => {
        regioninfo.data.tiles[tx][ty].content = old;
        this.world.markDirty(rx, ry, tx, ty);
        this.notify(ex);
      });
      return old;
    },
    spawnCursor(rx, ry, tx, ty) {
      this.world.ws.send({
        CreateCursor: [[rx, ry], [tx, ty]]
      }).catch(ex => {
        this.notify(ex);
      });;
    },
    deleteCursor(cursor) {
      let pos = [cursor.x,cursor.y].map(e => Math.floor(e / REGION_SIZE_TILES));
      this.world.ws.send({ DeleteCursor: [pos, cursor.id] }).then(() => {
        this.world.cursors.delete(cursor.id);
        this.world.markDirty();
      }).catch(ex => {
        this.notify(ex);
      });
    },
    async keydown(evt) {
      if (document.activeElement != document.body) return;

      let content;
      switch (evt.key) {
        case 'ArrowUp':
          do {
            content = await this.move(0, -1);
          } while(!is_empty(content) && evt.ctrlKey)
          this.charsTyped = 0;
          break;
        case 'ArrowLeft':
          do {
            content = await this.move(-1, 0);
          } while(!is_empty(content) && evt.ctrlKey)
          this.charsTyped = 0;
          break;
        case 'ArrowRight':
          do {
            content = await this.move(1, 0);
          } while(!is_empty(content) && evt.ctrlKey)
          this.charsTyped = 0;
          break;
        case 'ArrowDown':
          do {
            content = await this.move(0, 1);
          } while(!is_empty(content) && evt.ctrlKey)
          this.charsTyped = 0;
          break;
        case 'Home':
          this.move(-this.charsTyped, 0);
          this.charsTyped = 0;
          break;
        case 'End': /* ?? */ break;
        case 'PageUp': this.move(0, -32); this.charsTyped = 0; break;
        case 'PageDown': this.move(0, 32); this.charsTyped = 0; break;
        case 'Enter':
          this.move(-this.charsTyped, 1);
          this.charsTyped = 0;
          break;
        case 'Backspace':
          do {
            this.move(-1, 0);
            content = await this.updateTile(...this.cursor, null);
            this.charsTyped--;
          } while(!is_empty(content) && evt.ctrlKey)
          break;
        case 'Delete':
          do {
            content = await this.updateTile(...this.cursor, null);
            for (let cursor of this.world.cursors.values()) {
              let x = this.cursor[0]*REGION_SIZE_TILES + this.cursor[2];
              let y = this.cursor[1]*REGION_SIZE_TILES + this.cursor[3];
              if (cursor.x == x && cursor.y == y) {
                this.deleteCursor(cursor);
              }
            }
            this.move(1, 0);
            this.charsTyped++;
          } while(!is_empty(content) && evt.ctrlKey)
          break;
        default:
          if (evt.ctrlKey) return;
          if (evt.key.length > 1) return; // Don't preventDefault either
          this.updateTile(...this.cursor, evt.key);
          this.move(1, 0);
          this.charsTyped++;
          break;
      }
      evt.preventDefault();
    },
    async move(dx, dy) {
      this.cursor[2] += dx;
      this.cursor[3] += dy;
      while (this.cursor[2] >= REGION_SIZE_TILES) {
        this.cursor[2] -= REGION_SIZE_TILES;
        this.cursor[0] += 1;
      }
      while (this.cursor[2] < 0) {
        this.cursor[2] += REGION_SIZE_TILES;
        this.cursor[0] -= 1;
      }
      while (this.cursor[3] >= REGION_SIZE_TILES) {
        this.cursor[3] -= REGION_SIZE_TILES;
        this.cursor[1] += 1;
      }
      while (this.cursor[3] < 0) {
        this.cursor[3] += REGION_SIZE_TILES;
        this.cursor[1] -= 1;
      }

      let [rx, ry, tx, ty] = this.cursor;
      let region_size_px = this.tile_size_px * REGION_SIZE_TILES;
      let t_scroll_x = rx * region_size_px + tx * this.world.tile_size_px;
      let t_scroll_y = ry * region_size_px + ty * this.world.tile_size_px;
      let center_x = -(this.scroll_x - window.innerWidth / 2);
      let center_y = -(this.scroll_y - window.innerHeight / 2);
      let x_tolerance = window.innerWidth * .4;
      let y_tolerance = window.innerHeight * .4;
      if (t_scroll_x > center_x + x_tolerance) this.scroll_x -= (t_scroll_x - x_tolerance - center_x);
      if (t_scroll_x < center_x - x_tolerance) this.scroll_x -= (t_scroll_x + x_tolerance - center_x);
      if (t_scroll_y > center_y + y_tolerance) this.scroll_y -= (t_scroll_y - y_tolerance - center_y);
      if (t_scroll_y < center_y - y_tolerance) this.scroll_y -= (t_scroll_y + y_tolerance - center_y);

      this.world.cursor = this.cursor;
      this.world.markDirty();

      let regioninfo = await this.world.fetchRegionCt(rx, ry)
      if (!regioninfo) return 'None';
      return regioninfo.data.tiles[tx][ty].content;
    },

    register() {
      this.world.ws.send({
        Register: [this.username, this.password]
      }).then(() => {
        this.login();
        this.notify('registration successful');
      }).catch(ex => {
        this.notify(ex);
      });
    },
    login() {
      this.world.ws.send({
        Authenticate: [this.username, this.password]
      }).then(() => {
        this.notify('login successful');
      }).catch(ex => {
        this.notify(ex);
      });
    },

    notify(status) {
      let alert = { text: status, timer: 3000 };
      this.alerts.push(alert);

      let last = Date.now();
      let interval = setInterval(() => {
        let now = Date.now();
        alert.timer -= now - last;
        last = now;
        if (alert.timer <= 0) {
          this.alerts.splice(this.alerts.indexOf(alert), 1);
          clearInterval(interval);
        }
      }, 16);
    },

    setTool(tool, args) {
      console.log('tool set', tool, args);
      this.tool = tool;
      this.toolArgs = args;
    },
    tap(evt) {
      switch (this.tool) {
        case 'pan':
          let [rx, ry, tx, ty] = this.eventToCoordinates(evt);
          this.cursor = [rx, ry, tx, ty];
          this.world.cursor = this.cursor;
          this.world.markDirty();
          break;

        case 'claim': case 'unclaim': this.drag(evt); break;
        default: console.warn('unknown tool', this.tool);
      }
    },
    drag(evt) {
      let tile = this.eventToCoordinates(evt);
      switch (this.tool) {
        case 'pan':
          this.scroll_x += evt.delta.x;
          this.scroll_y += evt.delta.y;
          break;

        case 'claim':
          if (tile.toString() == this.lastDrag) break;
          let id = this.toolArgs;
          this.world.ws.send({
            Claim: [tile.slice(0,2), tile.slice(2,4), id]
          }).catch(ex => {
            this.notify('Failed to claim: ' + ex);
          });
          break;

        case 'unclaim':
          if (tile.toString() == this.lastDrag) break;
          let id2 = this.toolArgs;
          this.world.ws.send({
            Unclaim: [tile.slice(0,2), tile.slice(2,4), id2]
          }).catch(ex => {
            this.notify('Failed to unclaim: ' + ex);
          });
          break;

        default: console.warn('unknown tool', this.tool);
      }
      this.lastDrag = tile;
    }
  }
});
app.$mount('#app');
window.app = app;
