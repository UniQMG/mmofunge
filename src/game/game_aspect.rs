use crate::game::SaveJob;
use crate::util::database_manager::DatabaseManager;

pub trait GameAspect {
  fn tick(&self);

  /**
   * Restores the game aspect from the database
   */
  fn resurrect(&self);

  /**
   * Creates save jobs for the game aspect
   */
  fn save(&self) -> Box<dyn SaveJob + Send>;

  /**
   * Runs housekeeping tasks post-save such as region unloading.
   * Should be called immediately after saving, before the next tick.
   */
  fn post_save(&self);
}