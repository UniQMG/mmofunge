use std::collections::HashMap;
use std::ops::Deref;
use std::sync::{Arc, mpsc};
use std::sync::mpsc::{Receiver, RecvError, Sender, SyncSender};
use std::thread::{Builder, JoinHandle};

use crate::game::*;
use crate::util::LocalIdGenerator;
use crate::util::sync::*;

// extern crate no_deadlocks; use no_deadlocks::{ RwLock, Mutex }; // Now that's some drop-in debugging

/**
 * (region x, region y) => region manager, region transmitter
 *
 * The hashmap should only be write-locked by the world manager between ticks
 *   to facillitate loading or unloading new regions.
 * The region managers are for write-locking by the region worker during ticks.
 */
type Runners = Arc<RwLock<HashMap<RegionCoord, Arc<RwLock<RegionManager>>>>>;

enum RegionWorkerControl {
  RunStep(Step, SyncSender<()>),
  Save(Sender<RegionSaveJob>),
  PostSave(Sender<RegionCoord>),
  Abort
}

pub struct RegionWorkerThread {
  id: usize,
  runners: Runners,
  thread: JoinHandle<()>,
  tx: SyncSender<RegionWorkerControl>
}

impl RegionWorkerThread {
  pub fn new(id: usize, game: Arc<Game>) -> Self {
    let runners = Arc::new(RwLock::new(HashMap::new()));
    let (tx, rx) = mpsc::sync_channel(1);
    RegionWorkerThread {
      id: id,
      runners: runners.clone(),
      thread: Builder::new()
        .stack_size(4 * 1024 * 1024)
        .name(format!("Worker pool thread #{}", id))
        .spawn(move || RegionWorkerThread::run(id, game, runners, rx))
        .expect("Failed to spawn worker pool thread"),
      tx: tx
    }
  }

  pub fn id(&self) -> usize {
    self.id
  }

  pub fn run_step(&self, step: Step) -> Box<dyn FnOnce()> {
    let (tx, rx) = mpsc::sync_channel(1);
    self.tx.send(RegionWorkerControl::RunStep(step, tx)).expect("Worker thread is dead");
    Box::new(move || { rx.recv().expect("Worker thread hung up mid-operation"); })
  }
  pub fn save(&self, report_result: Sender<RegionSaveJob>) {
    self.tx.send(RegionWorkerControl::Save(report_result)).expect("Worker thread is dead");
  }
  pub fn post_save(&self, unloader: Sender<RegionCoord>) {
    self.tx.send(RegionWorkerControl::PostSave(unloader)).expect("Worker thread is dead");
  }

  fn block_until_loaded(region: &Arc<RwLock<RegionManager>>) {
    loop {
      let lock = region.read().unwrap();
      match lock.is_loaded() {
        true => return,
        false => {
          drop(lock);
          // println!("Waiting on region load");
          std::thread::sleep(std::time::Duration::from_millis(10));
        }
      }
    }
  }

  fn run(id: usize, game: Arc<Game>, runners: Runners, rx: Receiver<RegionWorkerControl>) {
    let id_gen = game.idgen();
    loop {
      match rx.recv() {
        Err(_) => panic!("Region worker manager hung up unexpectedly"),
        Ok(RegionWorkerControl::RunStep(step, tx)) => {
          for region in runners.read().unwrap().values() {
            RegionWorkerThread::block_until_loaded(region);
            RegionManager::run_step(
              step.mode(&game, region),
              RegionManagerUpdateArgs { id_generator: &id_gen }
            );
          }
          tx.send(()).unwrap();
        }
        Ok(RegionWorkerControl::Save(report_result)) => {
          let regions = runners.read().unwrap();
          for region in regions.values() {
            RegionWorkerThread::block_until_loaded(region);
            report_result.send(RegionSaveJob::new(region.read().unwrap().deref())).unwrap();
          }
        },
        Ok(RegionWorkerControl::PostSave(unload)) => {
          runners.write().unwrap().retain(|_key, region| {
            RegionWorkerThread::block_until_loaded(region);
            let region = region.read().unwrap();
            if region.can_unload() {
              unload.send(region.id()).unwrap();
              false
            } else {
              true
            }
          });
        }
        Ok(RegionWorkerControl::Abort) => break
      }
    }
    println!("Worker thread {} exited gracefully", id);
  }

  pub fn add_region(&self, manager: Arc<RwLock<RegionManager>>) {
    let id = manager.read().unwrap().id();
    self.runners.write().unwrap().insert(id, manager);
  }
}