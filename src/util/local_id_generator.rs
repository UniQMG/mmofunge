use crate::util::IdGenerator;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;

pub struct LocalIdGenerator {
  accumulator: AtomicUsize,
  worker_id: u64
}

impl LocalIdGenerator {
  pub fn new(worker_id: u64) -> Self {
    LocalIdGenerator {
      accumulator: AtomicUsize::new(0),
      worker_id
    }
  }

  pub fn generate(&self) -> u64 {
    IdGenerator::generate(self.worker_id, &self.accumulator)
  }
}