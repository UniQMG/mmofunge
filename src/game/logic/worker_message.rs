use std::sync::mpsc::Sender;

use serde::{Deserialize, Serialize};

use crate::game::*;

#[derive(Debug)]
pub enum WorkerMessage {
  Set(LocalTileCoord, TileContent, PermissionSet, Option<Confirm<()>>),
  /**
   * Requests a tile to be fetched (e.g. for the `g` instruction) and
   * returned via accompanying UpdateCursor message.
   * parameters: cursor id, return region x y, target tile x y
   */
  FetchRequest(CursorId, RegionCoord, GlobalTileCoord),
  /**
   * Fulfills a FetchRequest
   */
  FulfillFetchRequest(CursorId, TileContent),
  /**
   * Creates a new cursor, allocating an ID for it
   */
  CreateCursor(CursorState, GlobalTileCoord, Direction, Vec<CursorValue>),
  /**
   * Creates or updates a cursor
   */
  UpsertCursor(Cursor),
  /**
   * Locally removes a cursor without removing it from the database.
   * Paired with UpsertCursor for when cursors are moved (rather than deleted).
   */
  MoveCursorOut(CursorId),
  /**
   * Deletes a cursor. Queues it for deletion from the database during the
   * next save tick.
   */
  DeleteCursor(CursorId),
  GetState(Sender<Region>),
  Subscribe(Sender<RegionStateUpdate>),
  Claim(GlobalTileCoord, UserId, ClaimId, Confirm<()>),
  Unclaim(GlobalTileCoord, UserId, ClaimId, Confirm<()>),

  /**
   * Requests a value from an accompanying SyncSend instruction
   * parameters: cursor id, return location, target location
   */
  SyncRecieveRequest(CursorId, GlobalTileCoord, GlobalTileCoord),
  /**
   * Fulfills a SyncRecieveRequest
   */
  FulfillSyncRecieveRequest(CursorId, SyncRequestResponse)
}
