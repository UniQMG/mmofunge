export default class WebSocketManager {
  constructor(url) {
    this.ws = new WebSocket('ws://localhost:8001');
    this.ws.onmessage = this._onMessage.bind(this);
    this.ws_open = false;
    this.queue = [];
    this.ws.onopen = () => {
      this.ws_open = true;
      this.queue.forEach(packet => this.ws.send(packet));
      this.queue = null;
    }
    this.nonce_incr = 0;
    this.subscribers = new Map(); // event name -> handlers[]
    this.reply_promises = new Map(); // Nonce -> { resolve, reject }

    this.data_in = [];
    this.data_out = [];
  }

  data_report() {
    const window_size = 1000;
    while (this.data_in.length > 0 && Date.now() > this.data_in[0].when+window_size)
      this.data_in.shift();
    while (this.data_out.length > 0 && Date.now() > this.data_out[0].when+window_size)
      this.data_out.shift();
    let [ind, outd] = [this.data_in, this.data_out].map(arr => {
      if (arr.length == 0) return 0;
      let reduced = arr.reduce((acc, val) => acc + val.amount, 0)
      reduced /= window_size/1000;
      reduced /= 1024;
      reduced = Math.floor(reduced * 10) / 10;
      return reduced;
    });
    let [inp, outp] = [this.data_in.length, this.data_out.length];
    return `🡇 ${ind} KiB/s 🡅 ${outd} KiB/s`
  }

  send(payload) {
    let nonce = (++this.nonce_incr).toString();
    let packet = JSON.stringify([nonce, payload]);
    this.data_out.push({ amount: packet.length, when: Date.now() });
    if (!this.ws_open) {
      this.queue.push(packet);
    } else {
      this.ws.send(packet);
    }
    return new Promise((resolve, reject) => {
      this.reply_promises.set(nonce, { resolve, reject });
    });
  }

  _onMessage(evt) {
    this.data_in.push({ amount: evt.data.length, when: Date.now() });
    let data = JSON.parse(evt.data);

    if (data.Reply) {
      let [nonce, payload] = data.Reply;
      this.reply_promises.get(nonce).resolve(payload);
      this.reply_promises.delete(nonce);
      this._substream('Reply').forEach(ls => ls(nonce, payload));
    }
    if (data.Error) {
      let [nonce, payload] = data.Error;
      this.reply_promises.get(nonce).reject(payload);
      this.reply_promises.delete(nonce);
      this._substream('Error').forEach(ls => ls(nonce, payload));
    }
    if (data.FatalError) {
      console.error('Websocket Error:', data.FatalError)
      this._substream('FatalError').forEach(ls => ls(data.FatalError));
    }
    if (data.RatelimitWarning) {
      console.warn('Web socket ratelimit remaining: ' + data.RatelimitWarning);
    }
    if (data.Update) {
      this._substream('Update').forEach(ls => ls(data.Update));
    }
  }

  _substream(event) {
    if (!this.subscribers.has(event))
      this.subscribers.set(event, []);
    return this.subscribers.get(event);
  }

  subscribe(event, listener) {
    this._substream(event).push(listener);
  }
}
