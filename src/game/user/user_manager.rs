use std::collections::HashMap;
use std::ops::Deref;
use std::sync::Arc;

use mongodb::{bson, bson::{Bson, doc}, Database};

use crate::game::*;
use crate::GameAspect;
use crate::net::ClientError;
use crate::util::{IdGenerator, LocalIdGenerator};
use crate::util::database_manager::DatabaseManager;
use crate::util::sync::RwLock;

pub struct UserManager {
  pub users: RwLock<HashMap<UserId, Arc<RwLock<User>>>>,
  id_gen: LocalIdGenerator,
  dbm: Arc<DatabaseManager>
}

impl UserManager {
  pub fn new(id_gen: &IdGenerator, dbm: Arc<DatabaseManager>) -> Self {
    UserManager {
      users: RwLock::new(HashMap::new()),
      id_gen: id_gen.create_local(),
      dbm: dbm
    }
  }

  /**
   * Fetches a user, either returning the loaded version or fetching it from the database.
   * A None will be returned for an invalid user.
   */
  pub fn fetch_player(&self, id: UserId) -> Option<Arc<RwLock<User>>> {
    match self.users.read().unwrap().get(&id) {
      None => todo!(),
      Some(user) => Some(user.clone())
    }
  }

  pub fn register(&self, username: Username, password: Password) -> Result<(), ClientError> {
    let hash = User::hash_password(&password);

    let new_user = match self.authenticate_player(username, password) {
      AuthResult::AuthenticationFailed => return Err(ClientError::UsernameAlreadyRegistered),
      AuthResult::Ok(_) => return Err(ClientError::UsernameAlreadyRegistered),
      AuthResult::NoSuchUser => {
        let mut new_user = User::new(UserId(self.id_gen.generate()), username);
        new_user.password_hashed = hash;
        new_user
      }
    };

    let res = self.dbm.sync(self.dbm.database.collection("users").insert_one(
      DatabaseManager::insert_args(&new_user),
      None
    ));
    match res {
      Err(err) => {},
      Ok(res) => {}
    };
    Ok(())
  }

  /**
   * Fetches a user by username+password combo
   */
  pub fn authenticate_player(&self, username: Username, password: Password) -> AuthResult {
    let hash = User::hash_password(&password);

    let players = self.users.read().unwrap();
    for (_id, player) in players.iter() {
      let unplayer = player.read().unwrap();
      if unplayer.name != username { continue };
      if unplayer.password_hashed != hash {
        return AuthResult::AuthenticationFailed
      };
      return AuthResult::Ok(player.clone());
    }
    drop(players);

    let query = self.dbm.sync(self.dbm.database.collection("users").find_one(
      doc!{ "name": username.as_str(), "password_hashed": &hash },
      None
    ));

    let user: Arc<RwLock<User>> = match query {
      Err(err) => panic!("Database error {:?}", err),
      Ok(None) => return AuthResult::NoSuchUser,
      Ok(Some(val)) => {
        match bson::from_bson(Bson::Document(val)) {
          Err(err) => panic!("Database error {:?}", err),
          Ok(val) => Arc::new(RwLock::new(val))
        }
      }
    };

    let user_read = user.read().unwrap();
    if user_read.password_hashed != hash {
      return AuthResult::AuthenticationFailed;
    }
    let id = user_read.id;
    drop(user_read);

    let mut users = self.users.write().unwrap();
    users.insert(id, user.clone());
    AuthResult::Ok(user)
  }
}

impl GameAspect for UserManager {
  fn tick(&self) {
  }

  fn resurrect(&self) {
  }

  fn save(&self) -> Box<dyn SaveJob + Send> {
    Box::new(UserSaveJob::new(&self))
  }

  fn post_save(&self) {
  }
}
