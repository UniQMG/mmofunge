use std::ops::Deref;

use mongodb::bson::{self, doc, Document};
use mongodb::error::Error;
use mongodb::options::UpdateOptions;
use mongodb::results::UpdateResult;

use crate::game::{RegionSaveJob, SaveJob, User, UserManager};
use crate::util::DatabaseManager;

pub struct UserSaveJob {
  users: Vec<User>
}

impl UserSaveJob {
  pub fn new(user: &UserManager) -> Self {
    UserSaveJob {
      users: user.users
        .read()
        .unwrap()
        .values()
        .map(|user| user.read().unwrap().clone())
        .collect()
    }
  }
}

#[async_trait::async_trait]
impl SaveJob for UserSaveJob {
  async fn exec(&self, db: &DatabaseManager) -> Result<(), Error> {
    let col = db.database.collection("users");
    let futures = self.users.iter().map(|user| {
      col.update_one(
        doc!{ "id": user.id.0.to_string() },
        DatabaseManager::update_args(&user),
        UpdateOptions::builder().upsert(true).build()
      )
    }).collect::<Vec<_>>();
    for fut in futures { fut.await?; }
    Ok(())
  }
}