#[derive(Debug, serde::Serialize, serde::Deserialize, Clone, Copy, PartialEq)]
pub enum TileResource {
  Forest(u32)
}