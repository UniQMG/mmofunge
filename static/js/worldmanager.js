import Renderer from './renderer/renderer.js';
import WebSocketManager from './web_socket_manager.js';
import {REGION_SIZE_TILES} from './constants.js';

export default class WorldManager {
  constructor(canvas) {
    this.renderer = new Renderer(canvas);
    this.dirty = false;
    this.regions = new Map();
    this.cursors = new Map();
    this.scroll_x = 0;
    this.scroll_y = 0;
    this.tile_size_px = 32;
    this.cursor = [0, 0, 0, 0];
    this.ws = new WebSocketManager('ws://localhost:8001');
    this.user = null;
    this.ws.subscribe('Update', this._onupdate.bind(this));
    this.recomputeActiveRegions();

    this.drawLoop = this.drawLoop.bind(this);
    this.drawLoop();
  }

  drawLoop() {
    if (this.dirty) {
      this.dirty = false;
      this.redraw();
    }
    requestAnimationFrame(this.drawLoop);
  }

  redraw() {
    this.renderer.scroll_x = this.scroll_x;
    this.renderer.scroll_y = this.scroll_y;
    this.renderer.tile_size_px = this.tile_size_px;

    this.renderer.clear();
    this.renderer.drawRegions(this.regions.values());
    for (let cursor of this.cursors.values()) {
      this.renderer.drawCursor(cursor.x, cursor.y, cursor.state);
    }
    this.renderer.drawCursor(
      this.cursor[0] * REGION_SIZE_TILES + this.cursor[2],
      this.cursor[1] * REGION_SIZE_TILES + this.cursor[3],
      { Caret: true }
    );
  }

  /**
   * Marks part of the world dirty
   * @param {true|number} [rx] the region x, or true to mark all regions, or
   *   null to only trigger a redraw.
   * @param {number} [ry] the region y
   * @param {number} [tx] the tile x
   * @param {number} [ty] the tile y
   */
  async markDirty(rx=null, ry=null, tx=null, ty=null) {
    // TODO: Do we need to optimize the tx/ty specified case?
    if (rx === true) {
      for (let regioninfo of this.regions.values()) {
        this.renderer.markDirty(regioninfo);
      }
    }
    if (rx !== null && ry !== null) {
      let regioninfo = await this.fetchRegionCt(rx, ry);
      this.renderer.markDirty(regioninfo);
    }
    this.dirty = true;
  }

  async _onupdate(data) {
    if (data.Error) console.error(data);
    if (data.User) {
      this.user = data.User;
    }
    if (data.Full) {
      let [region, cursors] = data.Full;
      let key = `${region.x}_${region.y}`;

      if (!this.regions.has(key)) {
        this.regions.set(key, {
          state: 'loaded',
          error: null,
          data: region,
          promise: Promise.resolve(),
          x: region.x,
          y: region.y
        })
      }

      let regioninfo = this.regions.get(key);
      regioninfo.state = 'loaded';
      regioninfo.data = region;
      this.renderer.markDirty(regioninfo);
      this.dirty = true;
      for (let cursor of Object.values(cursors)) {
        this.cursors.set(cursor.id, cursor);
      }
    }
    if (data.CursorMessage) {
      let [id, msg] = data.CursorMessage;
      let cursor = this.cursors.get(id);
      cursor.log = (cursor.log || []).slice(-100);
      cursor.log.push(msg);
      this.dirty = true;
    }
    if (data.Cursor) {
      let cursor = data.Cursor;
      let existing = this.cursors.get(cursor.id);
      if (existing) {
        clearTimeout(existing.deleteTimeout);
        existing.deleteTimeout = null;
        Object.assign(existing, cursor);
      } else {
        this.cursors.set(cursor.id, cursor);
      }
      this.dirty = true;
    }
    if (data.CursorDeleted) {
      let cursor = this.cursors.get(data.CursorDeleted);
      if (!cursor || cursor.deleteTimeout) return;
      cursor.deleteTimeout = setTimeout(() => {
        this.cursors.delete(data.CursorDeleted);
        this.dirty = true;
      }, 1000);
    }
    if (data.Tile) {
      let [{ x, y }, { x: tx, y: ty }, tile] = data.Tile;
      let regioninfo = await this.fetchRegionCt(x, y);
      regioninfo.data.tiles[tx][ty] = tile;
      this.renderer.markDirty(regioninfo);
      this.dirty = true;
    }
  }

  async fetchRegionCt(x, y) {
    if (!this.regions.has(`${x}_${y}`)) {
      let promise_res = null;
      let promise_rej = null;
      let promise = new Promise((res, rej) => {
        promise_res = res;
        promise_rej = rej;
      });
      promise.resolve = promise_res;
      promise.reject = promise_rej;
      let region = {
        state: 'loading',
        error: null,
        data: null,
        promise,
        x,
        y
      };
      this.regions.set(`${x}_${y}`, region);
      this.ws.send({ Subscribe: [x, y] }).catch(ex => {
        region.state = 'failed';
        region.error = ex;
        promise.resolve();
      });
    }
    let region = this.regions.get(`${x}_${y}`);
    if (region.state != 'loaded')
      await region.promise;
    return region;
  }

  recomputeActiveRegions() {
    let region_size_px = this.tile_size_px * REGION_SIZE_TILES;
    let num_x = window.innerWidth / region_size_px;
    let num_y = window.innerHeight / region_size_px;
    let center_x = -this.scroll_x / region_size_px;
    let center_y = -this.scroll_y / region_size_px;
    let min_x = center_x - 1;
    let max_x = center_x + num_x + 1;
    let min_y = center_y - 1;
    let max_y = center_y + num_y + 1;
    let unload_rg = 1.5; // How many extra tiles away before *unloading* a region

    for (let region of this.regions.values()) {
      if (region.state == 'loading') continue;
      let x_in_range = region.x > min_x-unload_rg && region.x < max_x+unload_rg;
      let y_in_range = region.y > min_y-unload_rg && region.y < max_y+unload_rg;
      if (x_in_range && y_in_range) continue;
      if (region.state == 'loaded') {
        this.ws.send({ Unsubscribe: [region.x, region.y] });
      }
      this.regions.delete(`${region.x}_${region.y}`);
    }

    for (let fx = min_x; fx <= max_x; fx++) {
      for (let fy = min_y; fy <= max_y; fy++) {
        let x = Math.floor(fx), y = Math.floor(fy);
        if (!this.regions.has(`${x}_${y}`)) {
          this.fetchRegionCt(x, y);
        }
      }
    }
  }
}
