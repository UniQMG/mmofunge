use crate::game::*;
use serde::Deserialize;

pub type WsPacketIn = (Nonce, WsPacketInInner);

#[derive(Deserialize, Debug)]
pub enum WsPacketInInner {
  Register(Username, Password),
  Authenticate(Username, Password),
  Subscribe(RegionCoord), // region x y
  Unsubscribe(RegionCoord), // region x y
  Update(RegionCoord, LocalTileCoord, TileContent), // region x y, tile x y
  CreateCursor(RegionCoord, LocalTileCoord),
  DeleteCursor(RegionCoord, CursorId),
  CreateClaim,
  Claim(RegionCoord, LocalTileCoord, ClaimId),
  Unclaim(RegionCoord, LocalTileCoord, ClaimId)
}