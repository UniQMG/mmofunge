use std::collections::HashMap;

use mongodb::bson::{self, doc, Document};
use mongodb::error::Error;
use mongodb::options::UpdateOptions;
use mongodb::results::UpdateResult;

use crate::game::*;
use crate::util::DatabaseManager;

pub struct RegionSaveJob {
  region: Region,
  cursors: Vec<Cursor>
}

impl RegionSaveJob {
  pub fn new(region: &RegionManager) -> Self {
    RegionSaveJob {
      region: region.borrow_region().clone(),
      cursors: region.cursors.values().cloned().collect()
    }
  }
}

#[async_trait::async_trait]
impl SaveJob for RegionSaveJob {
  async fn exec(&self, db: &DatabaseManager) -> Result<(), Error> {
    for cursor in &self.cursors {
      match cursor.state {
        CursorState::QueuedForDeletion => {
          db.database.collection("cursors").delete_one(
            doc!{ "id": cursor.id.0.to_string() },
            None
          ).await?;
        },
        _ => {
          db.database.collection("cursors").update_one(
            doc! { "id": cursor.id.0.to_string() },
            DatabaseManager::update_args(&cursor),
            UpdateOptions::builder().upsert(true).build()
          ).await?;
        }
      }
    }

    db.database.collection("regions").update_one(
      doc!{ "x": self.region.pos.y, "y": self.region.pos.x },
      DatabaseManager::update_args(&self.region),
      UpdateOptions::builder().upsert(true).build()
    ).await?;
    Ok(())
  }
}