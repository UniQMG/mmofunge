const html = arg => arg.join('');
export default {
  template: html`
    <div class="sidebar">
      <h2>
        Cursor
        <a style="float: right" :href="\`#\${-cursor.x},\${-cursor.y}\`">
          ({{ cursor.x }}, {{ cursor.y }})
        </a>
      </h2>
      <div class="sidebar-content">
        <h3>ID</h3>
        {{ cursor.id }}
        <h3>State</h3>
        {{ cursor.state }}
        <div v-if="canDelete">
          <button @click="$emit('delete')">delete</button>
        </div>
        <h3>Log</h3>
        <div class="cursor-log" v-if="cursor.log">
          <span v-for="entry of cursor.log">{{ entry }}</span>
        </div>
        <h3>Stack (x{{ cursor.stack.length }})</h3>
        <div v-if="cursor.stack.length">
          -- bottom --
          <ul>
            <li v-for="(item, i) of cursor.stack" :key="i">
              {{ item }}
            </li>
          </ul>
          -- top --
        </div>
        <div v-else>
          Empty!
        </div>
      </div>
      <div class="sidebar-footer">
        <button @click="$emit('close')">x</button>
      </div>
    </div>
  `,
  props: ['cursor'],
  computed: {
    canDelete() {
      return (
        this.cursor.state.Dead ||
        this.cursor.state.SyncWriteBlock ||
        this.cursor.state == 'SyncReadBlock'
      );
    }
  }
}
