use arrayvec::ArrayString;
use serde::{Deserialize, Serialize};

use crate::game::REGION_SIZE;
use crate::util::serde_string;

pub type Username = ArrayString<16>;
pub type Password = String;

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Hash, Eq, PartialEq)]
pub struct Nonce(
  #[serde(with = "serde_string")]
  pub u64
);

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Hash, Eq, PartialEq)]
pub struct CursorId(
  #[serde(with = "serde_string")]
  pub u64
);

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Hash, Eq, PartialEq)]
pub struct UserId(
  #[serde(with = "serde_string")]
  pub u64
);

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Hash, Eq, PartialEq)]
pub struct ClaimId(
  #[serde(with = "serde_string")]
  pub u64
);

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Hash, Eq, PartialEq)]
#[derive(derive_more::Add, derive_more::Sub, derive_more::AddAssign, derive_more::SubAssign)]
pub struct RegionCoord {
  pub x: i64,
  pub y: i64
}
impl RegionCoord {
  /**
   * Converts region coordinates to global tile coordinates, given a local tile coordinate
   */
  pub fn to_global_tile(&self, rel: LocalTileCoord) -> GlobalTileCoord {
    GlobalTileCoord {
      x: (self.x * REGION_SIZE as i64) + rel.x as i64,
      y: (self.y * REGION_SIZE as i64) + rel.y as i64
    }
  }

  pub fn rel(&self, x: i64, y: i64) -> RegionCoord {
    RegionCoord { x: self.x + x, y: self.y + y }
  }
}
impl From<GlobalTileCoord> for RegionCoord {
  fn from(this: GlobalTileCoord) -> Self {
    this.to_region()
  }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Hash, Eq, PartialEq)]
#[derive(derive_more::Add, derive_more::Sub, derive_more::AddAssign, derive_more::SubAssign)]
pub struct LocalTileCoord {
  pub x: usize,
  pub y: usize
}
impl LocalTileCoord {
  /**
   * Checks if the tilecoordinate is within REGION_SIZE
   */
  pub fn is_in_bounds(&self) -> bool {
    self.x <= REGION_SIZE && self.y <= REGION_SIZE
  }
  /**
   * Converts local tile coordinates to global ones, given a region
   */
  pub fn to_global(&self, region: RegionCoord) -> GlobalTileCoord {
    GlobalTileCoord {
      x: region.x * REGION_SIZE as i64 + self.x as i64,
      y: region.y * REGION_SIZE as i64 + self.y as i64
    }
  }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize, Hash, Eq, PartialEq)]
#[derive(derive_more::Add, derive_more::Sub, derive_more::AddAssign, derive_more::SubAssign)]
pub struct GlobalTileCoord {
  pub x: i64,
  pub y: i64
}
impl GlobalTileCoord {
  /**
   * Converts global tile coordinates to local ones
   * Panics if the coordinates are not local to the given region
   */
  pub fn to_local(&self, region: RegionCoord) -> LocalTileCoord {
    let tile = LocalTileCoord {
      x: self.x.rem_euclid(REGION_SIZE as i64) as usize,
      y: self.y.rem_euclid(REGION_SIZE as i64) as usize
    };
    assert_eq!(tile.to_global(region), *self, "in region {:?} after intermediate {:?}", region, tile);
    tile
  }
  /**
   * Converts tile coordinates to the containing region coordinates
   */
  pub fn to_region(&self) -> RegionCoord {
    RegionCoord {
      x: self.x.div_euclid(REGION_SIZE as i64),
      y: self.y.div_euclid(REGION_SIZE as i64)
    }
  }
}
impl From<(i64, i64)> for GlobalTileCoord {
  fn from((x, y): (i64, i64)) -> Self {
    GlobalTileCoord { x, y }
  }
}

#[test]
fn unit_conversions() {
  let region = RegionCoord { x: 0, y: 0 };
  let local = LocalTileCoord { x: 5, y: 7 };
  let global = GlobalTileCoord { x: 5, y: 7 };
  assert_eq!(local.to_global(region), global);
  assert_eq!(global.to_local(region), local);
  assert_eq!(global.to_region(), region);
  assert_eq!(region.to_global_tile(local), global);

  let region = RegionCoord { x: -1, y: 0 };
  let local = LocalTileCoord { x: 12, y: 18 };
  let global = GlobalTileCoord { x: 12 - (REGION_SIZE as i64), y: 18 };
  assert_eq!(local.to_global(region), global);
  assert_eq!(global.to_local(region), local);
  assert_eq!(global.to_region(), region);
  assert_eq!(region.to_global_tile(local), global);

  let region = RegionCoord { x: 1, y: 0 };
  let local = LocalTileCoord { x: 15, y: 31 };
  let global = GlobalTileCoord { x: 15 + (REGION_SIZE as i64), y: 31 };
  assert_eq!(local.to_global(region), global);
  assert_eq!(global.to_local(region), local);
  assert_eq!(global.to_region(), region);
  assert_eq!(region.to_global_tile(local), global);

  let region = RegionCoord { x: -1, y: 0 };
  let local = LocalTileCoord { x: 31, y: 0 };
  let global = GlobalTileCoord { x: -1, y: 0 };
  assert_eq!(local.to_global(region), global);
  assert_eq!(global.to_local(region), local);
  assert_eq!(global.to_region(), region);
  assert_eq!(region.to_global_tile(local), global);
}
