use std::collections::HashMap;
use std::io::ErrorKind::WouldBlock;
use std::net::TcpListener;
use std::sync::{ Arc, mpsc };
use std::sync::mpsc::{Receiver, TryRecvError};
use std::thread::{Builder, spawn};

use serde::Deserialize;
use tungstenite::error::Error::*;
use tungstenite::Error::Io;
use tungstenite::protocol::Message;
use tungstenite::server::accept;

use crate::game::*;
use crate::net::*;
use tungstenite::error::ProtocolError;
use std::hint::unreachable_unchecked;


pub fn start_webserver(wm: Arc<Game>) {
  println!("Launching WS");

  let (tx, rx) = mpsc::channel::<Client>();

  Builder::new()
    .stack_size(4 * 1024 * 1024)
    .name("Websocket server".to_string())
    .spawn(move || {
      let mut clients = vec![];

      loop {
        // Register new clients
        loop {
          match rx.try_recv() {
            Err(TryRecvError::Disconnected) => panic!("ws server hung up"),
            Err(TryRecvError::Empty) => break,
            Ok(client) => clients.push(client)
          }
        }

        // Process existing clients
        clients.drain_filter(|client| {
          if client.is_dead() { println!("Client died"); return true }

          match client.tick() {
            Ok(()) => false,
            // Any unhandled error here is considered promoted to fatal
            Err(err) => {
              println!("Client dropped due to error: {:?}", err);
              if let Err(_err) = client.write(&WsPacketOut::FatalError(err)) {
                // welp
              }
              true
            }
          }
        });

        // Wait awhile before polling clients
        // I should probably learn async rust
        std::thread::sleep(std::time::Duration::from_millis(10));
      }
    })
    .unwrap();

  let server = TcpListener::bind(std::net::SocketAddr::from(([0, 0, 0, 0], 8001))).unwrap();
  'outer: for stream_res in server.incoming() {
    println!("Waiting for client...");
    let stream = stream_res.unwrap();
    stream.set_nonblocking(true).unwrap();
    let mut socket_res = accept(stream);
    let socket = loop {
      match socket_res {
        Ok(skt) => break skt,
        Err(tungstenite::handshake::HandshakeError::Interrupted(shake)) => {
          socket_res = shake.handshake();
          continue;
        },
        Err(err) => {
          println!("Client DC'd: {:?}", err);
          continue 'outer;
        }
      }
    };
    let client = Client::new(wm.clone(), socket);
    tx.send(client).unwrap();
    println!("Client accepted");
  }
}
