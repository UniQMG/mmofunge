use std::cell::Cell;
use std::sync::{Arc, mpsc};
use std::sync::mpsc::{Receiver, Sender, TryRecvError};
use std::time::Instant;

use tokio::task::JoinHandle;

use crate::game::*;
use crate::util::{BarStyle, DatabaseManager, IdGenerator, LocalIdGenerator, sync::Mutex};

/** The target time per tick, in milliseconds. */
const TICK_TIME: u128 = 100;
/** How many ticks between full world saves */
const SAVE_INTERVAL: usize = 1000;

type SaveTaskResult = Result<(), mongodb::error::Error>;

pub struct Game {
  pub world: WorldManager,
  pub user: UserManager,
  id_gen: IdGenerator,
  dbm: Arc<DatabaseManager>
}

impl Game {
  pub fn new(dbm: Arc<DatabaseManager>) -> Self {
    let id_gen = IdGenerator::new();
    Game {
      world: WorldManager::new(dbm.clone()),
      user: UserManager::new(&id_gen, dbm.clone()),
      id_gen: id_gen,
      dbm: dbm
    }
  }

  pub fn start(self: &Arc<Self>) {
    self.world.resurrect();
    self.user.resurrect();
    self.world.spawn(self, 12);
  }

  pub fn idgen(&self) -> LocalIdGenerator {
    self.id_gen.create_local()
  }

  pub fn run(&self) {
    let mut tick = 0;
    // let init_time = Instant::now();
    loop {
      let tick_start = Instant::now();

      tick += 1;
      self.user.tick();
      self.world.tick();

      if tick % SAVE_INTERVAL == 0 {
        self.dbm.sync(Game::run_save(self.dbm.clone(), vec![
          self.user.save(),
          self.world.save()
        ])).unwrap();
        self.user.post_save();
        self.world.post_save();
      }

      let elapsed = tick_start.elapsed().as_millis();
      let fillpct = elapsed as f64 / TICK_TIME as f64;
      let fill = crate::util::bar(16, fillpct, BarStyle::Shades);
      let regions = self.world.region_count();

      if elapsed < TICK_TIME {
        let delta = std::time::Duration::from_millis((TICK_TIME - elapsed) as u64);
        std::thread::sleep(delta);
      }

      println!(
        "Tick {:05} | {} regions | ({:3}ms / {}ms) [{}]",
        tick, regions, elapsed, TICK_TIME, fill
      );
      // let next_tick_time: u128 = tick as u128 * TICK_TIME;
      //
      // let ms_remaining = next_tick_time.saturating_sub(elapsed);
      //
      // let fillpct = 1.0 - (ms_remaining as f64 / TICK_TIME as f64);
      // let fill = crate::util::bar(16, fillpct, BarStyle::Shades);
      //
      // println!("Tick {:05} ({:3}ms / {}ms) [{}]", tick, TICK_TIME - ms_remaining, TICK_TIME, fill);
      // if next_tick_time > elapsed {
      //   let delta = std::time::Duration::from_millis((next_tick_time - elapsed) as u64);
      //   std::thread::sleep(delta);
      // }
    }
  }

  async fn run_save(dbm: Arc<DatabaseManager>, jobs: Vec<Box<dyn SaveJob + Send>>) -> SaveTaskResult {
    dbm.sanity_check().await;
    dbm.set_save_marker().await.unwrap();

    let mut tasks = Vec::with_capacity(jobs.len());
    for job in &jobs {
      tasks.push(job.exec(&dbm));
    }
    for task in tasks {
      task.await?;
    }

    dbm.remove_save_marker().await.unwrap();
    Ok(())
  }
}