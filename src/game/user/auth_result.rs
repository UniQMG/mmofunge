use std::sync::Arc;

use crate::game::User;
use crate::util::sync::RwLock;

pub enum AuthResult {
  NoSuchUser,
  AuthenticationFailed,
  Ok(Arc<RwLock<User>>)
}
