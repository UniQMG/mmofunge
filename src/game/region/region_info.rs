use std::sync::{Arc, mpsc::Sender};

use crate::game::*;
use crate::util::sync::*;

pub struct RegionInfo {
  pub manager: Arc<RwLock<RegionManager>>,
  pub sender: Arc<Mutex<Sender<WorkerMessage>>>,
  pub pool: Option<usize>,
}

impl RegionInfo {
  pub fn new(manager: RegionManager) -> Self {
    let sender = Arc::new(Mutex::new(manager.get_sender()));
    let arc_manager = Arc::new(RwLock::new(manager));

    RegionInfo {
      manager: arc_manager,
      sender: sender,
      pool: None
    }
  }

  pub fn id(&self) -> RegionCoord {
    let manager = self.manager.read().unwrap();
    manager.id()
  }

  pub fn assign_to_worker(&mut self, worker: &mut RegionWorkerThread) {
    assert!(self.pool == None);
    self.pool = Some(worker.id());
    worker.add_region(self.manager.clone());
  }
}
