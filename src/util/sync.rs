pub use std::sync::{Mutex, RwLock, RwLockReadGuard, RwLockWriteGuard};

// Now that's some drop-in debugging
// extern crate no_deadlocks;
// pub use no_deadlocks::{ RwLock, Mutex, RwLockReadGuard, RwLockWriteGuard };