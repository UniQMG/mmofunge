use crate::game::TileContent;

#[derive(Eq, PartialEq)]
pub enum Instruction {
  // Befunge native instructions
  Add,
  Sub,
  Mul,
  Div,
  Mod,
  LogicalNot,
  GreaterThan,
  DirRight,
  DirLeft,
  DirUp,
  DirDown,
  DirRandom,
  HorizontalIf,
  VerticalIf,
  StringMode,
  StackDuplicate,
  StackSwap,
  StackDiscard,
  StackOutputInteger,
  StackOutputChar,
  Bridge,
  Get,
  Put,
  StackInputInteger,
  StackInputChar,
  Terminate,
  PushInteger(u8),

  // Custom syntax extensions
  /// Toggles the cursor state to integer mode and pushes '0' to the stack
  /// While in integer mode, any PushInteger operations pop the stack value,
  /// multiply it by 10, and then add the PushInteger value and push it back.
  /// This allows for defining multi-character number literals
  IntegerMode,
  /// Pushes the current global tile x, y position of the cursor to the stack
  GetPosition,
  SyncSend,
  SyncRecieve,

  // Comment fallback
  Comment(char)
}

impl From<TileContent> for Instruction {
  fn from(content: TileContent) -> Instruction {
    match content {
      TileContent::Character(char) => char.into(),
      TileContent::None => Instruction::Comment(' ')
    }
  }
}

impl From<char> for Instruction {
  fn from(char: char) -> Self {
    use Instruction::*;
    match char {
      '+' => Add,
      '-' => Sub,
      '*' => Mul,
      '/' => Div,
      '%' => Mod,
      '!' => LogicalNot,
      '`' => GreaterThan,
      '>' => DirRight,
      '<' => DirLeft,
      '^' => DirUp,
      'v' => DirDown,
      '?' => DirRandom,
      '_' => HorizontalIf,
      '|' => VerticalIf,
      '"' => StringMode,
      ':' => StackDuplicate,
      '\\' => StackSwap,
      '$' => StackDiscard,
      '.' => StackOutputInteger,
      ',' => StackOutputChar,
      '#' => Bridge,
      'g' => Get,
      'p' => Put,
      '&' => StackInputInteger,
      '~' => StackInputChar,
      '@' => Terminate,
      '0' => PushInteger(0),
      '1' => PushInteger(1),
      '2' => PushInteger(2),
      '3' => PushInteger(3),
      '4' => PushInteger(4),
      '5' => PushInteger(5),
      '6' => PushInteger(6),
      '7' => PushInteger(7),
      '8' => PushInteger(8),
      '9' => PushInteger(9),

      '=' => GetPosition,
      '\'' => IntegerMode,
      '[' => SyncSend,
      ']' => SyncRecieve,

      char => Comment(char)
    }
  }
}
