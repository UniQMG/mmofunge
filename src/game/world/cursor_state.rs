use serde::{Deserialize, Serialize};

use crate::game::CursorValue;

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub enum CursorState {
  /** The cursor is executing instructions and moving normally */
  Normal,
  /** The cursor will interpret all characters as a string literal until the next " */
  StringMode,
  /** The cursor will interpret digits as multi-character numeric literals until the next ' */
  IntegerMode,
  /** The cursor is awaiting a FulfillFetchRequest */
  FetchRequestBlock,
  /** The cursor is awaiting (first parameter)x FulfillSyncRecieveRequests */
  SyncReadBlock(i64),
  /** The cursor is awaiting a SyncRecieveRequest to reply to */
  SyncWriteBlock(CursorValue),
  /** The cursor is dead and will no longer execute */
  Dead(DeathReason),
  /** The cursor is queued for deletion and will be removed from the database next save */
  QueuedForDeletion
}

#[derive(Debug, Clone, Copy, Serialize, Deserialize, PartialEq)]
pub enum DeathReason {
  StackOverflow,
  StackUnderflow,
  TypeMismatch,
  Terminated,
  ReadWriteDenied,
  OutOfBounds,
  Drowned
}