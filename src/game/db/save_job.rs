use mongodb::error::Error;

use crate::util::DatabaseManager;

#[async_trait::async_trait]
pub trait SaveJob {
  async fn exec(&self, db: &DatabaseManager) -> Result<(), Error>;
}

