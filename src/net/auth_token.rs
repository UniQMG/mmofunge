use arrayvec::ArrayString;
use rand::Rng;
use std::fmt::Write;

pub type AuthToken = ArrayString<32>;

pub fn gen_random() -> AuthToken {
  let mut str = ArrayString::new();

  for _ in 0..str.capacity() {
    write!(str, "{:X}", rand::thread_rng().gen_range(0..=0xF)).unwrap();
  }

  str
}

#[test]
fn auth_token_test() {
  assert_eq!(gen_random().len(), 32);
}