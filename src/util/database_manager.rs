use mongodb::{
  Client,
  Database,
  error::Error,
  bson::{self, Bson, doc, Document},
  results::{UpdateResult, DeleteResult, InsertOneResult},
  options::{FindOneOptions, UpdateOptions}
};
use std::{
  future::Future,
  sync::{Arc, Mutex, atomic::AtomicUsize},
  thread::Builder,
  collections::HashMap
};
use tokio::{
  task::JoinHandle,
  runtime::{Runtime, Handle},
  stream::{Stream, StreamExt}
};
use crate::game::Cursor;
use crate::game::CursorId;
use std::sync::atomic::Ordering;
use std::thread::spawn;
use serde::Serialize;

pub struct DatabaseManager {
  runtime: Arc<Runtime>,
  handle: Handle,
  client: Client,
  pub database: Database
}

impl DatabaseManager {
  pub fn new(mongo_url: &str) -> Self {
    let mut rt = tokio::runtime::Builder::new()
      .threaded_scheduler()
      .core_threads(4)
      .thread_name("tokio-db")
      .enable_all()
      .build()
      .unwrap();
    let client = rt
      .block_on(async { Client::with_uri_str(mongo_url).await })
      .expect("Failed to connect to database");
    let db = client.database("mmofunge");

    DatabaseManager {
      handle: rt.handle().clone(),
      runtime: Arc::new(rt),
      client: client,
      database: db
    }
  }

  pub fn insert_args<T: Serialize>(t: &T) -> Document {
    match bson::to_bson(t) {
      Ok(bson::Bson::Document(doc)) => doc,
      Err(err) => panic!("Serialization failed {:?}", err),
      Ok(v) => panic!("Unexpected {:?}", v)
    }
  }

  pub fn update_args<T: Serialize>(t: &T) -> Document {
    return doc!{ "$set": DatabaseManager::insert_args(t) }
  }

  /**
   * Seeing as the mongodb rust driver doesn't support transactions yet, this will have to do.
   */
  pub async fn sanity_check(&self) {
    let marker_query = self.database.collection("config").find_one(
      doc!{ "id": "SAVE_MARKER" },
      None
    ).await;
    match marker_query {
      Err(err) => panic!("Database error {:?}", err),
      Ok(Some(_val)) => panic!("Database inconsistent: save marker present"),
      Ok(None) => {},
    };
  }

  pub async fn set_save_marker(&self) -> Result<InsertOneResult, Error> {
    self.database.collection("config").insert_one(
      doc!{ "id": "SAVE_MARKER", "note": "save in progress" },
      None
    ).await
  }

  pub async fn remove_save_marker(&self) -> Result<DeleteResult, Error> {
    self.database.collection("config").delete_one(
      doc! { "id": "SAVE_MARKER" },
      None
    ).await
  }

  pub fn coalesce_one<T, V: for<'de> serde::Deserialize<'de>> (
    &self,
    query: impl Future<Output=Result<Option<Document>, Error>>,
    mapper: &impl Fn(V) -> T,
    default: &impl Fn() -> T
  ) -> T {
    match self.sync(query) {
      Err(err) => panic!("Database error {:?}", err),
      Ok(None) => default(),
      Ok(Some(val)) => {
        match bson::from_bson(Bson::Document(val)) {
          Err(err) => panic!("Database error {:?}", err),
          Ok(val) => mapper(val)
        }
      }
    }
  }

  pub fn background<T: 'static + Send>(&self, future: impl Future<Output=T> + Send + 'static) -> JoinHandle<T> {
    self.handle.spawn(future)
  }

  pub async fn async_str<T>(&self, mut stream: impl Stream<Item=T> + std::marker::Unpin) -> Vec<T> {
    let mut vals = vec![];
    while let Some(val) = stream.next().await {
      vals.push(val);
    }
    vals
  }

  pub fn sync<T>(&self, future: impl Future<Output=T>) -> T {
    self.handle.block_on(future)
  }

  pub fn sync_str<T>(&self, stream: impl Stream<Item=T> + std::marker::Unpin) -> Vec<T> {
    self.handle.block_on(self.async_str(stream))
  }
}