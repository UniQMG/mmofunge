#[derive(Debug, serde::Serialize, serde::Deserialize, Clone, Copy, PartialEq)]
pub enum TileMaterial {
  Snow,
  Rock,
  Grass,
  Dirt,
  Sand,
  Water(f64), // depth
  None,
  AbstractElevation(f64)
}

impl TileMaterial {
  pub fn from_elevation(elevation: f64) -> Self {
    // return TileMaterial::AbstractElevation(elevation);
    match elevation {
      e if e <  0.450 => TileMaterial::Water(0.450 - e),
      e if e <  0.500 => TileMaterial::Sand,
      e if e <  0.800 => TileMaterial::Grass,
      e if e <  0.900 => TileMaterial::Rock,
      e if e <= 1.000 => TileMaterial::Snow,
      _ => TileMaterial::Snow
    }
  }
}
