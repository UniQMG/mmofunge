#[derive(Debug, serde::Serialize, serde::Deserialize, Clone, Copy, PartialEq)]
pub enum TileContent {
  Character(char),
  None
}
