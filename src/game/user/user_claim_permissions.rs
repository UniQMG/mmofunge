use arrayvec::ArrayVec;

use crate::game::{ClaimId, ClaimPermission};

#[derive(Debug, Clone, serde::Serialize, serde::Deserialize)]
pub struct UserClaimPermissions {
  claims: ArrayVec<(ClaimPermission, ClaimId), 10>
}

impl UserClaimPermissions {
  pub fn new() -> Self {
    UserClaimPermissions { claims: ArrayVec::new() }
  }

  pub fn set_perms(&mut self, query: ClaimId, level: ClaimPermission) -> Result<(), ()> {
    match self.get_perms(query) {
      existing if existing == level => return Ok(()),
      _ if level != ClaimPermission::None => self.set_perms(query, ClaimPermission::None)?,
      _ => {}
    }

    match level {
      ClaimPermission::None => {
        for x in (0..self.claims.len()).rev() {
          if self.claims[x].1 == query {
            self.claims.remove(x);
          }
        }
        Ok(())
      },
      level => {
        if self.claims.is_full() {
          return Err(());
        }
        self.claims.push((level, query));
        Ok(())
      }
    }
  }

  pub fn get_perms(&self, query: ClaimId) -> ClaimPermission {
    self.claims.iter()
      .filter(|(_perm, claim)| query == *claim)
      .map(|(perm, _claim)| perm)
      .fold(ClaimPermission::None, |a, b| if a > *b { a } else { *b })
  }
}

#[test]
fn claim_permission_levels() {
  let mut claims = UserClaimPermissions::new();
  claims.set_perms(ClaimId(0), ClaimPermission::Owner);
  claims.set_perms(ClaimId(0), ClaimPermission::Member);
  claims.set_perms(ClaimId(1), ClaimPermission::Member);
  claims.set_perms(ClaimId(1), ClaimPermission::Owner);
  claims.set_perms(ClaimId(2), ClaimPermission::Owner);
  claims.set_perms(ClaimId(2), ClaimPermission::None);
  assert_eq!(claims.get_perms(ClaimId(0)), ClaimPermission::Member);
  assert_eq!(claims.get_perms(ClaimId(1)), ClaimPermission::Owner);
  assert_eq!(claims.get_perms(ClaimId(2)), ClaimPermission::None);
  assert_eq!(claims.get_perms(ClaimId(3)), ClaimPermission::None);
}