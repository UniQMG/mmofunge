#![feature(proc_macro_hygiene, decl_macro, drain_filter)]
#[macro_use] extern crate mongodb;
#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

use std::sync::{Arc, mpsc};
use std::thread::{Builder, spawn};

use mongodb::bson::doc;
use mongodb::Client;
use rocket::http::Status;
use rocket::State;
use rocket_contrib::databases::mongodb::db::ThreadedDatabase;
use rocket_contrib::json::Json;
use rocket_contrib::serve::StaticFiles;

use game::*;
use net::*;
use util::database_manager::DatabaseManager;

mod game;
mod util;
mod net;

#[database("db")]
struct DbConn(mongodb_rocket::db::Database);


fn main() {
  print!("\x1B[2J");
  let server = rocket::ignite()
    .attach(DbConn::fairing())
    .mount("/", StaticFiles::from("./static"));

  let mongo_url = server.config().extras
    .get("databases").expect("Database not configured")
    .get("db").expect("Database config invalid: missing db")
    .get("url").expect("Database config invalid: missing db.url")
    .as_str().expect("Database config invalid");

  let dbm = DatabaseManager::new(mongo_url);
  dbm.sync(dbm.sanity_check());

  let game = Builder::new()
    .stack_size(4 * 1024 * 1024)
    .name("Init thread".to_string())
    .spawn(move || {
      let game = Arc::new(Game::new(Arc::new(dbm)));
      game.start();
      game
    })
    .unwrap()
    .join()
    .unwrap();


  let game1 = game.clone();
  let wm_thread = Builder::new()
    .stack_size(4 * 1024 * 1024)
    .name("Game thread".to_string())
    .spawn(move || game1.run())
    .unwrap();

  let game2 = game.clone();
  Builder::new()
    .name("Websocket".to_string())
    .spawn(move || start_webserver(game2))
    .unwrap();

  let game3 = game.clone();
  Builder::new()
    .name("Rocket".to_string())
    .spawn(move || server.manage(game3).launch())
    .unwrap();

  wm_thread.join().unwrap();
}