use std::ops::Deref;
use std::sync::mpsc::Sender;

use super::RegionStateUpdate;

pub struct WorkerSubs {
  subscribers: Vec<Sender<RegionStateUpdate>>
}
impl WorkerSubs {
  pub fn new() -> Self {
    WorkerSubs { subscribers: vec![] }
  }

  pub fn subcount(&mut self) -> usize {
    self.notify(RegionStateUpdate::NoOp);
    self.subscribers.len()
  }

  pub fn register(&mut self, sender: Sender<RegionStateUpdate>) {
    self.subscribers.push(sender);
  }

  pub fn notify(&mut self, update: RegionStateUpdate) {
    self.subscribers.retain(|sub| {
      match sub.send(update.clone()) {
        Ok(()) => true,
        Err(_) => false
      }
    });
  }
}
