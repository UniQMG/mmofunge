use std::sync::atomic::{AtomicUsize, Ordering, AtomicU64};
use std::time::{SystemTime, UNIX_EPOCH, Duration};
use crate::util::LocalIdGenerator;
use std::sync::Arc;
use std::sync::atomic::Ordering::SeqCst;

pub struct IdGenerator {
  workers: AtomicU64
}

const BITS_TIMESTAMP: u64 = 42;
const BITS_MACHINE: u64   = 10;
const BITS_SEQUENCE: u64  = 12;

const SHIFT_TIMESTAMP: u64 = BITS_MACHINE + BITS_SEQUENCE;
const SHIFT_MACHINE: u64   = BITS_SEQUENCE;
const SHIFT_SEQUENCE: u64  = 0;

const MASK_TIMESTAMP: u64 = 2_u64.pow(BITS_TIMESTAMP as u32) - 1;
const MASK_MACHINE: u64   = 2_u64.pow(BITS_MACHINE   as u32) - 1;
const MASK_SEQUENCE: u64  = 2_u64.pow(BITS_SEQUENCE  as u32) - 1;

impl IdGenerator {
  pub fn new() -> IdGenerator {
    IdGenerator { workers: AtomicU64::new(0) }
  }

  pub fn create_local(&self) -> LocalIdGenerator {
    LocalIdGenerator::new(self.workers.fetch_add(1, SeqCst))
  }

  pub fn generate(worker_id: u64, acc: &AtomicUsize) -> u64 {
    let timestamp = SystemTime::now()
      .duration_since(UNIX_EPOCH + Duration::from_secs(1609488000))
      .expect("Epoch fail")
      .as_millis();
    let machine = worker_id;
    let sequence = acc.fetch_add(1, Ordering::SeqCst) as u64 % MASK_SEQUENCE;


    if timestamp > MASK_TIMESTAMP as u128 {
      panic!("ID generator date out of range");
    }
    if machine > MASK_MACHINE {
      panic!("ID generator worker out of range");
    }
    let timestamp = timestamp as u64;

    (timestamp & MASK_TIMESTAMP) << SHIFT_TIMESTAMP |
    (machine   &   MASK_MACHINE) <<   SHIFT_MACHINE |
    (sequence  &  MASK_SEQUENCE) <<  SHIFT_SEQUENCE
  }
}