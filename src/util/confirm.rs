use std::sync::mpsc::{self, Sender, Receiver};
use crate::net::ClientError;

#[derive(Debug)]
pub struct Confirm<T> {
  tx: Sender<Result<T, ClientError>>,
  confirmed: bool
}

impl<T: 'static> Confirm<T> {
  pub fn new() -> (Confirm<T>, Box<dyn FnOnce() -> Result<T, ClientError>>) {
    let (tx, rx) = mpsc::channel::<Result<T, ClientError>>();
    let cb = move || {
      match rx.recv() {
        Err(_) => Err(ClientError::InternalError("Confirmer hung up")),
        Ok(result) => result
      }
    };
    (Confirm { tx, confirmed: false }, Box::new(cb))
  }

  pub fn confirm(mut self, result: T) {
    self.confirmed = true;
    let _ = self.tx.send(Ok(result));
  }

  pub fn fail(mut self, err: ClientError) {
    self.confirmed = true;
    let _ = self.tx.send(Err(err));
  }
}

impl<T> Drop for Confirm<T> {
  fn drop(&mut self) {
    if !self.confirmed {
      panic!("Confirmer dropped before confirming");
    }
  }
}