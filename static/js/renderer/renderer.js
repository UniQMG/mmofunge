import RegionCanvas from './region_canvas.js';
import * as constants from '../constants.js';

export default class Renderer {
  constructor(canvas) {
    this.canvas = canvas;
    this.canvases = new WeakMap();
    this.scroll_x = 0;
    this.scroll_y = 0;
    this.tile_size_px = 32;
  }

  markDirty(region) {
    this.getRegionCanvas(region).dirty = true;
  }

  clear() {
    let ctx = this.canvas.getContext('2d');
    ctx.fillStyle = '#CCC';
    ctx.fillRect(0, 0, this.canvas.width, this.canvas.height);
  }

  drawCursor(cx, cy, state) {
    if (state == 'QueuedForDeletion') return;

    let ctx = this.canvas.getContext('2d');
    ctx.save();
    ctx.translate(this.scroll_x, this.scroll_y);
    ctx.translate(cx * this.tile_size_px, cy * this.tile_size_px);
    let fill = false;

    ctx.lineWidth = 2;
    if (state.Caret) {
      ctx.strokeStyle = 'red';
      ctx.fillStyle = 'yellow';
      fill = true;
    }
    else if (state == 'Normal') {
      ctx.strokeStyle = 'green';
    }
    else if (state.Dead) {
      ctx.strokeStyle = 'gray';
    }
    else if (state == 'StringMode') {
      ctx.strokeStyle = 'cyan';
    }
    else if (state == 'IntegerMode') {
      ctx.strokeStyle = 'blue';
    }
    else if (state == 'FetchRequestBlock') {
      ctx.strokeStyle = 'red';
    }
    else {
      ctx.strokeStyle = 'black';
    }


    ctx.strokeRect(0, 0, this.tile_size_px, this.tile_size_px);
    if (fill) {
      ctx.globalAlpha = 0.4;
      ctx.fillRect(0, 0, this.tile_size_px, this.tile_size_px);
    }
    ctx.restore();
  }

  drawRegions(regioninfos) {
    let ctx = this.canvas.getContext('2d');
    ctx.save();
    ctx.translate(this.scroll_x, this.scroll_y);
    for (let regioninfo of regioninfos) {
      let w = constants.REGION_SIZE_TILES * this.tile_size_px;
      let h = constants.REGION_SIZE_TILES * this.tile_size_px;
      let x = w * regioninfo.x;
      let y = h * regioninfo.y;

      switch (regioninfo.state) {
        default:
          ctx.fillStyle = '#777';
          ctx.fillRect(x, y, w, h);
          break;

        case 'loaded':
          let rcanvas = this.getRegionCanvas(regioninfo);
          rcanvas.redrawIfDirty(regioninfo.data, this.tile_size_px);
          ctx.drawImage(rcanvas.getCanvas(), x, y, w, h);
          break;

        case 'failed':
          ctx.fillStyle = '#700';
          ctx.fillRect(x, y, w, h);
          ctx.fillStyle = 'black';
          ctx.fillText(regioninfo.error, x, y+12);
          break;
      }
    }
    ctx.restore();
  }

  getRegionCanvas(regioninfo) {
    if (!this.canvases.has(regioninfo)) {
      this.canvases.set(regioninfo, new RegionCanvas());
    }
    return this.canvases.get(regioninfo);
  }
}
