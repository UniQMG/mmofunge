use arrayvec::{ArrayString, ArrayVec};
use serde::{Deserialize, Serialize};

use crate::game::*;

#[derive(Serialize, Deserialize, Clone)]
pub struct User {
  pub id: UserId,
  pub name: Username,
  pub claim_blocks: i64,
  pub password_hashed: String,
  pub cursors: ArrayVec<CursorId, 10>,
  #[serde(flatten)]
  pub claims: UserClaimPermissions
}

impl User {
  pub fn new(id: UserId, name: ArrayString<16>) -> Self {
    User {
      id: id,
      name: name,
      claim_blocks: 10,
      password_hashed: "totally a legitimate hash function".parse().unwrap(),
      cursors: ArrayVec::new(),
      claims: UserClaimPermissions::new()
    }
  }

  pub fn hash_password(password: &Password) -> String {
    "totally a legitimate hash function ".to_owned() + password
  }
}