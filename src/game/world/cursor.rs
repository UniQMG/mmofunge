use serde::{Deserialize, Serialize};

use crate::game::{CursorId, CursorValue, Direction, GlobalTileCoord};
use crate::game::world::cursor_state::{CursorState, DeathReason};
use crate::game::world::cursor_value::CursorValue::*;

const CURSOR_STACK_LIMIT: usize = 1000;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Cursor {
  pub id: CursorId,
  pub state: CursorState,
  #[serde(flatten)]
  pub pos: GlobalTileCoord,
  pub dir: Direction,
  pub stack: Vec<CursorValue>
}

impl Cursor {
  /**
   * Steps the cursor once in its current direction
   */
  pub fn step(&mut self) {
    self.pos += self.dir.delta().into();
  }

  pub fn kill(&mut self, reason: DeathReason) {
    self.state = CursorState::Dead(reason);
  }

  /**
   * Attempts to push a value to the cursor's stack, killing it if it would overflow.
   */
  pub fn sepushku(&mut self, val: CursorValue) {
    if self.stack.len() >= CURSOR_STACK_LIMIT {
      self.kill(DeathReason::StackOverflow);
      return
    }
    self.stack.push(val);
  }

  /**
   * Attempts to pop a value from the cursor's stack, killing it if it underflows.
   */
  pub fn sepopku(&mut self, callback: &impl Fn(&mut Self, CursorValue)) {
    match self.stack.pop() {
      Some(val) => callback(self, val),
      None => self.kill(DeathReason::StackUnderflow)
    }
  }

  /**
   * Attempts to pop two values from the cursor's stack, killing it if it underflows.
   */
  pub fn double_sepopku(&mut self, callback: &impl Fn(&mut Self, CursorValue, CursorValue)) {
    match (self.stack.pop(), self.stack.pop()) {
      (Some(a), Some(b)) => callback(self, a, b),
      _ => self.kill(DeathReason::StackUnderflow)
    }
  }

  /**
   * Attempts to pop three values from the cursor's stack, killing it if it underflows.
   */
  pub fn triple_sepopku(&mut self, callback: &impl Fn(&mut Self, CursorValue, CursorValue, CursorValue)) {
    match (self.stack.pop(), self.stack.pop(), self.stack.pop()) {
      (Some(a), Some(b), Some(c)) => callback(self, a, b, c),
      _ => self.kill(DeathReason::StackUnderflow)
    }
  }

  /**
   * Attempts to pop two integers from the stack and push a derived value back.
   * Kills the cursor on failure (type mismatch or underflow).
   */
  pub fn fold_two_ints(&mut self, callback: &impl Fn(i64, i64) -> i64) {
    match (self.stack.pop(), self.stack.pop()) {
      (Some(Integer(a)), Some(Integer(b))) => self.stack.push(Integer(callback(a, b))),
      (Some(_), Some(_)) => self.kill(DeathReason::TypeMismatch),
      _ => self.kill(DeathReason::StackUnderflow)
    }
  }
}
