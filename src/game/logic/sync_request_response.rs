use crate::game::CursorValue;

#[derive(Debug, Copy, Clone, serde::Serialize, serde::Deserialize)]
pub enum SyncRequestResponse {
  Success(CursorValue),
  NoInstruction
}