use arrayvec::ArrayVec;
use serde::{Deserialize, Serialize};

use crate::game::{ClaimId, TileContent, TileMaterial, TileResource};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Tile {
  pub content: TileContent,
  pub material: TileMaterial,
  pub resources: ArrayVec<TileResource, 5>,
  pub claim: Option<ClaimId>
}

impl Tile {
  pub fn new() -> Self {
    Tile {
      content: TileContent::None,
      material: TileMaterial::None,
      resources: ArrayVec::new(),
      claim: None
    }
  }
}
