const html = arg => arg.join('');
export default {
  template: html`
    <div class="sidebar" v-if="!user">
      <h2>
        User
      </h2>
      Not logged in
    </div>
    <div class="sidebar" v-else>
      <h2>
        User
      </h2>
      <div class="sidebar-content">
        <h3>ID</h3>
        {{ user.id }}
        <h3>Name</h3>
        {{ user.name }}
        <h3>Claims</h3>
        <div>Claim blocks: {{ user.claim_blocks }}</div>
        <div>Claims: {{ user.claims.length }} / 10</div>
        <ul>
          <li v-for="([owner, id], i) of user.claims" :key="i">
            {{ owner }} of {{ id }}
            <button @click="$emit('setTool', 'claim', id)">claim</button><!--
            --><button @click="$emit('setTool', 'unclaim', id)">unclaim</button><!--
            --><button @click="alert('not yet implemented')">leave</button>
          </li>
        </ul>
        <button @click="createclaim" :disabled="user.claims.length >= 10">+</button>
      </div>
      <div class="sidebar-footer">
        <button @click="$emit('close')">x</button>
      </div>
    </div>
  `,
  props: ['world'],
  computed: {
    user() { return this.world.user; }
  },
  methods: {
    alert(msg) { alert(msg) },
    async createclaim() {
      let id = await this.world.ws.send('CreateClaim').catch(ex => {
        this.$root.notify('Failed to create claim: ' + ex);
      });
      console.log('created claim', id);
    }
  }
}
