use crate::util::sync::*;
use crate::game::*;
use crate::net::*;
use tungstenite::{WebSocket, Message};
use tungstenite::error::Error as TgError;
use std::net::TcpStream;
use std::sync::{Arc, mpsc::{self, Receiver, Sender}};
use std::collections::HashMap;
use std::sync::mpsc::TryRecvError;
use serde::Serialize;
use std::time::{Instant, Duration};
use std::ops::Add;
use crate::util::LocalIdGenerator;

const SUBSCRIPTION_LIMIT: u64 = 256;
const RATELIMIT_BURST: u64 = 100000; // max accumulated packet burst
const RATELIMIT_PACKETS: u64 = 100; // packets per interval
const RATELIMIT_INTERVAL: u64 = 1; // seconds
const VIOLATION_CAP: u64 = 1000; // max violations before disconnect

pub struct Client {
  manager: Arc<Game>,
  id_gen: LocalIdGenerator,
  listeners: HashMap<RegionCoord, Receiver<RegionStateUpdate>>,
  socket: WebSocket<TcpStream>,
  user: Option<Arc<RwLock<User>>>,
  ratelimit_remaining: u64,
  ratelimit_violations: u64,
  last_tick: Instant
}

impl Client {
  pub fn new(game: Arc<Game>, ws: WebSocket<TcpStream>) -> Self {
    Client {
      id_gen: game.idgen(),
      manager: game,
      listeners: HashMap::new(),
      socket: ws,
      user: None,
      ratelimit_remaining: RATELIMIT_BURST,
      ratelimit_violations: 0,
      last_tick: Instant::now()
    }
  }

  fn region(&self, pos: RegionCoord) -> Result<Arc<RwLock<RegionManager>>, ClientError> {
    Ok(self.manager.world.fetch_region(pos).as_ref().ok_or(ClientError::OutOfBounds)?.clone())
  }

  fn authenticate(&self) -> Result<&Arc<RwLock<User>>, ClientError> {
    self.user.as_ref().ok_or(ClientError::NotAuthenticated)
  }

  fn sender(&self, pos: RegionCoord) -> Result<Sender<WorkerMessage>, ClientError> {
    self.manager.world.get_sender(pos).ok_or(ClientError::OutOfBounds)
  }

  pub fn write(&mut self, val: &WsPacketOut) -> Result<(), ClientError> {
    let msg = serde_json::to_string(val)?;
    match self.socket.write_message(Message::Text(msg)) {
      Err(TgError::Io(err)) if err.kind() == std::io::ErrorKind::WouldBlock => Ok(()), // is queued
      Err(err) => Err(err.into()),
      Ok(()) => Ok(())
    }
  }

  fn write_user_update(&mut self) -> Result<(), ClientError> {
    let user = self.authenticate()?;
    let mut user = user.read()?.clone();
    user.password_hashed = "$2a$10$euQ/BVeAfYwH8TQ4IBDb2efXKcy051qTmbRjmWfZxXhFGezv6iTie".to_owned();
    self.write(&WsPacketOut::Update(RegionStateUpdate::User(user)))
  }

  pub fn is_dead(&self) -> bool {
    !self.socket.can_write()
  }

  /**
   * Ticks the client, processing any inbound messages and flushing
   * the subscription queue.
   */
  pub fn tick(&mut self) -> Result<(), ClientError> {
    while self.last_tick.elapsed().as_secs() >= RATELIMIT_INTERVAL {
      self.last_tick = self.last_tick.add(Duration::new(RATELIMIT_INTERVAL, 0));
      self.ratelimit_remaining += RATELIMIT_PACKETS;
      if self.ratelimit_remaining > RATELIMIT_BURST {
        self.ratelimit_remaining = RATELIMIT_BURST;
        self.ratelimit_violations = self.ratelimit_violations.saturating_sub(1);
      }
    }

    loop {
      match self.socket.read_message() {
        Ok(msg) => {
          self.ratelimit_remaining = self.ratelimit_remaining.saturating_sub(1);
          if self.ratelimit_remaining == 0 && self.ratelimit_violations > VIOLATION_CAP {
            return Err(ClientError::Ratelimited);
          }

          let (nonce, packet): WsPacketIn = serde_json::from_str(msg.to_text()?)?;
          // println!("[nonce {}]: {:?}", nonce.0, packet);
          match self.ratelimit_remaining {
            0 => {
              self.ratelimit_violations += 1;
              self.write(&WsPacketOut::Error(nonce, ClientError::Ratelimited))?;
              continue;
            },
            x if (x < 500 && x % 100 == 0) || (x < 50 && x % 10 == 0) || (x < 10) => {
              self.write(&WsPacketOut::RatelimitWarning(self.ratelimit_remaining))?;
            },
            _ => {}
          }

          let reply = match self.process_packet(packet) {
            Err(err) if err.is_fatal() => return Err(err),
            Err(err) => WsPacketOut::Error(nonce, err),
            Ok(res) => WsPacketOut::Reply(nonce, res)
          };
          self.write(&reply)?;
        },
        Err(TgError::Io(err)) if err.kind() == std::io::ErrorKind::WouldBlock => {
          self.flush_queue()?;
          return Ok(())
        },
        Err(err) => {
          return Err(err.into())
        }
      }
    }
  }

  /**
   * Reads the client's subscription queue, removing expired subscriptions, and
   * pushes events to the client.
   */
  fn flush_queue(&mut self) -> Result<(), ClientError> {
    let mut to_remove = vec![];
    let mut packets = vec![];
    for (id, rx) in self.listeners.iter() {
      loop {
        match rx.try_recv() {
          Err(TryRecvError::Disconnected) => to_remove.push(*id),
          Err(TryRecvError::Empty) => break,
          Ok(val) => packets.push(WsPacketOut::Update(val))
        }
      }
    }
    for packet in packets {
      self.write(&packet)?;
    }
    for id in to_remove {
      self.listeners.remove(&id);
    }
    Ok(())
  }

  /**
   * Processes the client's packet
   */
  fn process_packet(&mut self, packet: WsPacketInInner) -> Result<WsPacketOutInner, ClientError> {
    match packet {
      WsPacketInInner::Register(username, password) => {
        self.manager.user.register(username, password)?;
      },
      WsPacketInInner::Authenticate(username, password) => {
        self.user = match self.manager.user.authenticate_player(username, password) {
          AuthResult::AuthenticationFailed => return Err(ClientError::InvalidCredentials),
          AuthResult::NoSuchUser => return Err(ClientError::InvalidCredentials),
          AuthResult::Ok(user) => Some(user)
        };
        self.write_user_update()?;
      }
      WsPacketInInner::Subscribe(region) => {
        if self.listeners.len() >= SUBSCRIPTION_LIMIT as usize {
          return Err(ClientError::SubscriptionLimitReached);
        }
        let (tx, rx) = mpsc::channel();
        let worker = self.region(region)?;
        let worker = worker.read()?;
        self.sender(region)?.send(WorkerMessage::Subscribe(tx))?;
        drop(worker);
        self.listeners.insert(region, rx);
      },
      WsPacketInInner::Unsubscribe(region) => {
        drop(self.listeners.remove(&region));
      },
      WsPacketInInner::Update(region, tile, content) => {
        if !tile.is_in_bounds() { return Err(ClientError::OutOfBounds); }
        let user = self.authenticate()?;
        let (confirm, confirmer) = Confirm::new();
        let perms = PermissionSet::Explicit(user.read().unwrap().claims.clone());
        self.sender(region)?.send(WorkerMessage::Set(tile, content, perms, Some(confirm)))?;
        confirmer()?;
      },
      WsPacketInInner::CreateCursor(region, tile) => {
        let user = self.authenticate()?;
        self.sender(region)?.send(WorkerMessage::CreateCursor(
          CursorState::Normal,
          tile.to_global(region),
          Direction::Right,
          vec![]
        ))?;
      },
      WsPacketInInner::DeleteCursor(region, id) => {
        let user = self.authenticate()?;
        self.sender(region)?.send(WorkerMessage::DeleteCursor(id))?;
      }
      WsPacketInInner::CreateClaim => {
        let id = ClaimId(self.id_gen.generate());
        let mut user = self.authenticate()?.write()?;
        if user.claims.set_perms(id, ClaimPermission::Owner).is_err() {
          return Err(ClientError::ResourceLimitReached);
        }
        drop(user);
        self.write_user_update()?;
        return Ok(WsPacketOutInner::ClaimCreated(id));
      },
      WsPacketInInner::Claim(region, tile, claim) => {
        let mut user = self.authenticate()?.write()?;
        let query = claim;
        if user.claims.get_perms(query) < ClaimPermission::Manager {
          return Err(ClientError::InsufficientPermission)
        }
        if user.claim_blocks <= 0 {
          return Err(ClientError::ResourceLimitReached);
        }

        let (confirmer, confirm) = Confirm::new();
        let msg = WorkerMessage::Claim(tile.to_global(region), user.id, claim, confirmer);
        self.sender(region)?.send(msg)?;
        confirm()?;

        user.claim_blocks -= 1;
        drop(user);
        self.write_user_update()?;
      },
      WsPacketInInner::Unclaim(region, tile, claim) => {
        let mut user = self.authenticate()?.write()?;
        let query = claim;
        if user.claims.get_perms(query) < ClaimPermission::Manager {
          return Err(ClientError::InsufficientPermission)
        }

        let (confirmer, confirm) = Confirm::new();
        let msg = WorkerMessage::Unclaim(tile.to_global(region), user.id, claim, confirmer);
        self.sender(region)?.send(msg)?;
        confirm()?;

        user.claim_blocks += 1;
        drop(user);
        self.write_user_update()?;
      }
    };
    Ok(WsPacketOutInner::Success)
  }
}
