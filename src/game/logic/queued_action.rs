use serde::{Deserialize, Serialize};

use crate::game::{CursorId, CursorValue, GlobalTileCoord, RegionCoord, SyncRequestResponse, TileContent};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum QueuedAction {
  FulfillFetchRequest(RegionCoord, CursorId, TileContent),
  /**
   * Fulfills a sync request.
   */
  FulfillSyncRequest(RegionCoord, CursorId, SyncRequestResponse),
  /**
   * Retries a sync request next tick
   */
  RetrySyncRequest(CursorId, GlobalTileCoord, GlobalTileCoord)
}
