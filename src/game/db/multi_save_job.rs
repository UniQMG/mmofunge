use mongodb::error::Error;

use crate::game::SaveJob;
use crate::util::DatabaseManager;

pub struct MultiSaveJob<T: SaveJob + Sync> {
  jobs: Vec<T>
}

impl<T: SaveJob + Sync> MultiSaveJob<T> {
  pub fn new(initial_capacity: usize) -> Self {
    MultiSaveJob { jobs: Vec::with_capacity(initial_capacity) }
  }

  pub fn push(&mut self, t: T) {
    self.jobs.push(t);
  }
}

#[async_trait::async_trait]
impl<T: SaveJob + Sync> SaveJob for MultiSaveJob<T> {
  async fn exec(&self, db: &DatabaseManager) -> Result<(), Error> {
    let mut futures = Vec::with_capacity(self.jobs.len());

    for job in &self.jobs {
      futures.push(job.exec(db))
    }

    for job in futures {
      job.await?
    }

    Ok(())
  }
}