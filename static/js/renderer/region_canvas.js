import * as constants from '../constants.js';

function image(src) {
  let img = new Image();
  img.src = src;
  return img;
}
const images = {
  Snow:  image('img/tex/Snow.png'),
  Water: image('img/tex/Water.png'),
  Rock:  image('img/tex/Rock.png'),
  Sand:  image('img/tex/Sand.png'),
  Grass: image('img/tex/Grass.png'),
}

export default class RegionCanvas {
  constructor() {
    this.canvas = document.createElement('canvas');
    this.dirty = true;
  }

  getCanvas() {
    return this.canvas;
  }

  redrawIfDirty(region, tile_size_px) {
    if (!this.dirty) return;
    this.dirty = false;

    this.canvas.width = constants.REGION_SIZE_TILES * tile_size_px;
    this.canvas.height = constants.REGION_SIZE_TILES * tile_size_px;
    let ctx = this.canvas.getContext('2d');

    ctx.save();
    ctx.scale(tile_size_px, tile_size_px);
    for (let x = 0; x < constants.REGION_SIZE_TILES; x++) {
      for (let y = 0; y < constants.REGION_SIZE_TILES; y++) {
        let tile = region.tiles[x][y];

        let type = typeof tile.material == 'string'
          ? tile.material
          : Object.keys(tile.material)[0];
        let brightness = x % 2 == y % 2 ? 1.0 : 0.95;

        switch (type) {
          case 'None': ctx.fillStyle = 'white'; break;
          case 'Snow': ctx.fillStyle = 'lightgray'; break;
          case 'Rock': ctx.fillStyle = 'gray'; break;
          case 'Grass': ctx.fillStyle = 'green'; break;
          case 'Sand': ctx.fillStyle = '#ffc70f'; break;
          case 'Water':
            let depth = tile.material.Water;
            ctx.fillStyle = `rgb(0, 0, ${255 - depth*500})`;
            brightness = 1 - (depth * 2);
            break;
          case 'AbstractElevation': // for debugging
            let val = tile.material.AbstractElevation * 2;
            if (val < 0) {
              ctx.fillStyle = 'indigo';
            } else if (val <= 1) {
              ctx.fillStyle = `rgb(0, 0, ${val * 255})`;
            } else if (val <= 2) {
              val -= 1;
              ctx.fillStyle = `rgb(0, ${val * 255}, ${(1-val) * 255})`;
            } else if (val <= 3) {
              val -= 2;
              ctx.fillStyle = `rgb(${val * 255}, ${(1-val) * 255}, 255)`
            } else {
              ctx.fillStyle = 'orange';
            }
            break;
          default: throw new Error('Unknown tile material: ' + type);
        }

        if (images[type]) {
          ctx.imageSmoothingEnabled = false;
          ctx.save();
          ctx.translate(x, y);
          ctx.translate(.5, .5);
          ctx.rotate(Math.floor(Math.random() * 4)/4 * Math.PI * 2);
          ctx.translate(-.5, -.5);
          ctx.drawImage(images[type], -0.01, -0.01, 1.02, 1.02);
          ctx.restore();
        } else {
          ctx.fillRect(x, y, 1, 1);
        }

        ctx.globalAlpha = 1 - brightness;
        ctx.fillStyle = 'black';
        ctx.fillRect(x, y, 1, 1);
        ctx.globalAlpha = 1.0;

        if (tile.claim) {
          let hue = tile.claim.slice(-10) % 360;
          ctx.fillStyle = `hsl(${hue}, 100%, 50%, .25)`;
          ctx.fillRect(x, y, 1, 1);
        }

        if (tile.predicted) {
          ctx.strokeStyle = 'red';
          ctx.lineWidth = 2/tile_size_px;
          ctx.strokeRect(x, y, 1, 1);
        }

        if (tile.content.Character) {
          ctx.fillStyle = 'black';

          ctx.save();
          ctx.font = '1px sans-serif';
          ctx.textAlign = 'center';
          ctx.textBaseline = 'middle';
          ctx.fillText(tile.content.Character, x+.5, y+.5);
          ctx.restore();
        }
      }
    }
    ctx.restore();
    // ctx.lineWidth = 2;
    // ctx.strokeStyle = 'red';
    // ctx.strokeRect(0, 0, this.canvas.width, this.canvas.height);
  }
}
