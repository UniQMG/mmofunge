use std::collections::HashMap;
use std::ops::{Add, Div, Mul};

use noise::{NoiseFn, OpenSimplex, Seedable, SuperSimplex};

use WorldGenOp::*;

use crate::game::*;

const MASTER_SCALE: f64 = 0.05;

enum Distribution {
  Pow(f64)
}
impl Distribution {
  fn apply(&self, val: f64) -> f64 {
    match self {
      Distribution::Pow(pow) => val.powf(*pow)
    }
  }
}
enum WorldGenOp {
  // seed, scale, amplitude, lowpass, highpass, distribution
  Noise(SuperSimplex, f64, f64, f64, f64, Distribution),
  // range
  Blur(i64),
  Mul(f64)
}
#[derive(Clone)]
struct GenTile {
  elevation: f64,
  pos: GlobalTileCoord,
  material: Option<TileMaterial>
}
fn seed(seed: u32) -> SuperSimplex {
  SuperSimplex::new().set_seed(seed)
}

pub struct WorldGenerator {
  ops: Vec<WorldGenOp>
}

impl WorldGenerator {
  pub fn new() -> Self {
    WorldGenerator {
      ops: vec![
        //        (    seed), scale,   amp, lo_p, hi_p
        Noise(seed(82489645),  30.0,  0.50, 0.00, 0.20, Distribution::Pow( 6.0)),
        Noise(seed(39585381),   5.0,  0.30, 0.00, 0.50, Distribution::Pow( 5.0)),
        Noise(seed(68497152),   2.5,  0.20, 0.00, 1.00, Distribution::Pow( 1.0)),
        Noise(seed(32858953),   1.0,  0.10, 0.00, 1.00, Distribution::Pow( 1.0)),
        Noise(seed(48785193),   0.5,  0.10, 0.00, 1.00, Distribution::Pow( 1.0)),
        Noise(seed(23598123),   1.0,  0.10, 0.00, 0.30, Distribution::Pow( 0.5)),
        Mul(1.0/1.3),
        Noise(seed(21348575),  30.0,  0.50, 0.50, 1.00, Distribution::Pow(4.0)),
        Noise(seed(35839852),  30.0,  0.20, 0.00, 1.00, Distribution::Pow(4.0)),
        Blur(3),
      ]
    }
  }

  /**
   * Generates a region given coordinates
   */
  pub fn generate(&self, pos: RegionCoord) -> Region {
    let spawn_radi = 1;
    if pos.x >= -spawn_radi && pos.x <= spawn_radi && pos.y >= -spawn_radi && pos.y <= spawn_radi {
      return Region::new(pos);
    }

    // Generate a padded area before sticking it into a region
    // to allow for operations that read from neighboring cells to tile safely
    let padding = 4;

    let mut tiles = vec![];
    for x in 0..REGION_SIZE+padding*2 {
      let mut vec = vec![];
      for y in 0..REGION_SIZE+padding*2 {
        vec.push(GenTile {
          pos: pos.to_global_tile(LocalTileCoord { x, y }),
          elevation: 0.0,
          material: None
        });
      }
      tiles.push(vec);
    }

    for op in &self.ops {
      let tileswap = tiles.clone();
      for (x, row) in tiles.iter_mut().enumerate() {
        for (y, tile) in row.iter_mut().enumerate() {
          match op {
            Noise(noise, scale, amp, lowpass, highpass, dstb) => {
              let pt = [
                tile.pos.x as f64 * 1.0 / scale * MASTER_SCALE,
                tile.pos.y as f64 * 1.0 / scale * MASTER_SCALE
              ];

              let mut value = noise.get(pt);
              value = value.add(1.0).div(2.0); // -1..1 -> 0..1
              value = dstb.apply(value).clamp(*lowpass, *highpass);

              tile.elevation += ((value - lowpass) / (highpass - lowpass)) * amp;
            }
            Blur(range) => {
              let mut total = 0.0;
              let mut visited = 0;
              for dx in -range..*range {
                for dy in -range..*range {
                  let tx = (x as i64) + dx;
                  let ty = (y as i64) + dy;
                  if tx.is_negative() || ty.is_negative() { continue };
                  let tile = tileswap.get(tx as usize).and_then(|f| f.get(ty as usize));
                  if let Some(tile) = tile {
                    total += tile.elevation;
                    visited += 1;
                  }
                }
              }
              tile.elevation = total / visited as f64;
            }
            Mul(mul) => {
              tile.elevation *= mul;
            }
          }
        }
      }
    }

    let mut region = Region::new(pos);
    for x in 0..REGION_SIZE {
      for y in 0..REGION_SIZE {
        let gentile = &tiles[x + padding][y + padding];
        region.tiles[x][y].material = TileMaterial::from_elevation(gentile.elevation);
      }
    }
    region
  }
}