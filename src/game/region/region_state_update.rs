use std::collections::HashMap;

use serde::Serialize;

use crate::game::*;

#[derive(Serialize, Clone)]
pub enum RegionStateUpdate {
  User(User),
  Full(Region, HashMap<CursorId, Cursor>),
  Cursor(Cursor),
  CursorDeleted(CursorId),
  CursorMessage(CursorId, String),
  Tile(RegionCoord, LocalTileCoord, Tile), // region x y, tile x y, tile
  NoOp
}